﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Pebble.Domain.Flows;
using Pebble.Domain.Models;
using Pebble.Domain.Services;
using Pebble.MainWebSite.Utils;

namespace Pebble.MainWebSite.Controllers
{
	public class FileUploadController : PebbleController
	{
		public IFlow Flow { get; set; }
		public IAvatarProcessor AvatarProcessor { get; set; }
		public IDocumentProcessor DocumentProcessor { get; set; }
		public IFileProcessor FileProcessor { get; set; }

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult GroupUploadUserAvatar(HttpPostedFileBase FileUpload)
		{
			var error = string.Empty;
            var responseObject = new Dictionary<string, object> { { "AvatarUrl", "" }, { "RawFileName", "" }, { "Error", "" } };
            if (!AvatarProcessor.ValidateAvatarSize(ApplicationId, FileUpload.ContentLength, ref error))
            {
                responseObject["Error"] = error;
                return Json(responseObject, "text/plain");
            }

            var fileName = DocumentProcessor.SanitizeFilename(Path.GetFileName(FileUpload.FileName));
            var mimeType = DocumentProcessor.MimeTypeFromFileNameExtension(fileName);

            if (!DocumentProcessor.ValidateFileType(mimeType, ref error))
            {
                responseObject["Error"] = error;
                return Json(responseObject, "text/plain");
            }

            if (!DocumentProcessor.ValidateImage(FileUpload.InputStream, ref error))
            {
                responseObject["Error"] = error;
                return Json(responseObject, "text/plain");
            }

			return Content(new JavaScriptSerializer().Serialize(AvatarProcessor.ResizeImageForCropping(FileUpload.InputStream)), "text/plain");
		}

        [HttpPost, ValidateAntiForgeryToken, NoCache]
        public ActionResult ShowProfileImageForCropping(HttpPostedFileBase FileUpload)
        {
            var error = string.Empty;
            if (!AvatarProcessor.ValidateAvatarSize(ApplicationId, FileUpload.ContentLength, ref error))
            {
                return Content(new JavaScriptSerializer().Serialize(new ImageCropDetails { Error = error }), "text/plain");
            }

            var fileName = DocumentProcessor.SanitizeFilename(Path.GetFileName(FileUpload.FileName));
            var mimeType = DocumentProcessor.MimeTypeFromFileNameExtension(fileName);

            if (!DocumentProcessor.ValidateFileType(mimeType, ref error))
            {
                return Content(new JavaScriptSerializer().Serialize(new ImageCropDetails { Error = error }), "text/plain");
            }

            if (!DocumentProcessor.ValidateImage(FileUpload.InputStream, ref error))
            {
                return Content(new JavaScriptSerializer().Serialize(new ImageCropDetails { Error = error }), "text/plain");
            }

            return Content(new JavaScriptSerializer().Serialize(AvatarProcessor.ResizeImageForCropping(FileUpload.InputStream)), "text/plain");
        }

		[HttpPost]
		public ActionResult CropImage(string filename, int x1, int y1, int x2, int y2, ImageCropSource cropSource)
		{
			var imageCropDetails = AvatarProcessor.CropSelectedArea(filename, x1, y1, x2, y2, ApplicationId, UserId);
			imageCropDetails.UserId = UserId;
			Flow.User.SetUserHasAvatar(ApplicationId, UserId);
			return Json(new { imageCropDetails.AvatarUrl, imageCropDetails.RawFileName, UserId, error = string.Empty });
		}

		[HttpPost]
		public ActionResult CropAndSaveTempImage(string filename, int x1, int y1, int x2, int y2, ImageCropSource cropSource)
		{
			var imageCropDetails = AvatarProcessor.CropSelectedAreaTemp(filename, x1, y1, x2, y2, ApplicationId, UserId, cropSource);
			imageCropDetails.UserId = string.Empty;
			return Json(new { imageCropDetails.AvatarUrl, imageCropDetails.RawFileName, string.Empty, error = string.Empty });
		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult MessageDocuments(HttpPostedFileBase DocumentUpload, HttpPostedFileBase ReplyDocumentUpload, HttpPostedFileBase ConvDocumentUpload)
		{
			//TODO: find a better way
			DocumentUpload = DocumentUpload ?? ReplyDocumentUpload;
			DocumentUpload = DocumentUpload ?? ConvDocumentUpload;

			var error = string.Empty;
			var responseObject = new Dictionary<string, object> { { "FileUrl", "" }, { "RawFileName", "" }, { "FileName", "" }, { "HasPreview", "" }, { "error", "" } };
            //if (!DocumentProcessor.ValidateDocumentSize(ApplicationId, DocumentUpload.ContentLength, ref error))
            //{
            //    responseObject["error"] = error;
            //    return Json(responseObject, "text/plain");
            //}

            var fileName = DocumentProcessor.SanitizeFilename(Path.GetFileName(DocumentUpload.FileName));
            var mimeType = DocumentProcessor.MimeTypeFromFileNameExtension(fileName);

            //if (!DocumentProcessor.ValidateFileType(mimeType, ref error))
            //{
            //    responseObject["error"] = error;
            //    return Json(responseObject, "text/plain");
            //}

            //if (mimeType.IsImage && !DocumentProcessor.ValidateImage(DocumentUpload.InputStream, ref error))
            //{
            //    responseObject["error"] = error;
            //    return Json(responseObject, "text/plain");
            //}

            //if (mimeType.IsImage && !DocumentProcessor.ValidateImageAttachmentDimensions(DocumentUpload.InputStream, ref error))
            //{
            //    responseObject["error"] = error;
            //    return Json(responseObject, "text/plain");
            //}

		    string base64Data;
		    using (var m = new MemoryStream())
		    {
                DocumentUpload.InputStream.Seek(0, SeekOrigin.Begin);
                DocumentUpload.InputStream.CopyTo(m);
                base64Data = Convert.ToBase64String(m.ToArray());
                m.Close();
		    }

		    var fileUploadResponse = mimeType.IsImage 
                ? FileProcessor.UploadImage(new ImageFileToUpload { EncodedContent = base64Data, FileName = fileName, FlipType = RotateFlipType.RotateNoneFlipNone }, FileUploadType.Documents, ApplicationId, ref error) 
                : FileProcessor.UploadNonImage(new ImageFileToUpload { EncodedContent = base64Data, FileName = fileName, FlipType = RotateFlipType.RotateNoneFlipNone }, FileUploadType.Documents, ApplicationId, ref error);

			if (fileUploadResponse == null)
			{
				responseObject["error"] = error;
				return Json(responseObject, "text/plain");
			}

			responseObject["FileUrl"] = fileUploadResponse.FileUrl;
			responseObject["RawFileName"] = fileUploadResponse.RawFileName;
			responseObject["FileName"] = fileUploadResponse.FileName;
			responseObject["HasPreview"] = fileUploadResponse.HasPreview;

			return Json(responseObject, "text/plain");
		}
	}
}