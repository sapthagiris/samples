﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Pebble.Domain.Exceptions;
using Pebble.Domain.Flows;
using Pebble.Domain.Models;
using Pebble.Domain.Search;
using Pebble.Domain.Services;
using Pebble.Domain.Services.Extensions;
using Pebble.MainWebSite.Models;
using Pebble.MainWebSite.Utils;
using Pebble.Resources;
using PlainElastic.Net;

namespace Pebble.MainWebSite.Controllers
{
	[ClockinCheck, TnCAcceptedCheck, SignupCompletionCheck("JoinGroup, LeaveGroup")]
	public class GroupsController : PebbleController
	{
		public IFlow Flow { get; set; }
		public IAvatarProcessor AvatarProcessor { get; set; }
		public IMessageProcessor MessageProcessor { get; set; }

		[PebbleControllerLogging]
		public virtual ActionResult Home(string id, bool? groupInviteError)
		{
            return View("~/Views/Groups/Home.cshtml", new GroupsListModel { UserId = id ?? string.Empty, GroupListType = GroupListType.MyGroups, SearchSortBy = GroupSortOptions.MemberCount, SearchFilter = AlphaBetaFilter.None, Suggestions = null, GroupInviteError = groupInviteError ?? false });
		}

        [PebbleControllerLogging]
        public virtual ActionResult MyGroups(string id)
        {
            return View("~/Views/Groups/Home.cshtml", new GroupsListModel { UserId = id ?? string.Empty, GroupListType = GroupListType.MyGroups, SearchSortBy = GroupSortOptions.MemberCount, SearchFilter = AlphaBetaFilter.None, Suggestions = null, GroupInviteError = false });
        }

        [PebbleControllerLogging]
        public virtual ActionResult AllGroups(string id)
        {
            return View("~/Views/Groups/Home.cshtml", new GroupsListModel { UserId = id ?? string.Empty, GroupListType = GroupListType.AllGroups, SearchSortBy = GroupSortOptions.MemberCount, SearchFilter = AlphaBetaFilter.None, Suggestions = null, GroupInviteError = false });
        }

        [PebbleControllerLogging]
        public virtual ActionResult TheirGroups(string id, GroupsListModel model)
        {
            var userName = string.Empty;
            var groupType = GroupListType.MyGroups;
            if (!string.IsNullOrEmpty(id) && UserId != id)
            {
                groupType = GroupListType.TheirGroups;
                var user = Flow.User.GetUserDetails(ApplicationId, id);
                if (user != null) userName = user.FirstName + "'s " + Groups.GroupsTitle;
            }
            return View("~/Views/Groups/Home.cshtml", new GroupsListModel { UserId = id ?? string.Empty, GroupListType = groupType, SearchSortBy = GroupSortOptions.MemberCount, SearchFilter = AlphaBetaFilter.None, Suggestions = null, GroupInviteError = false, UserName = userName });
        }

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GroupList(string id, GroupsListModel model)
		{
			var error = string.Empty;
			if (!string.IsNullOrWhiteSpace(model.SearchText) && (MessageProcessor.HasHtmlContent(model.SearchText, ref error) || MessageProcessor.HasXss(model.SearchText, ref error) || MessageProcessor.HasPds(model.SearchText, ref error)))
			{
				Response.StatusCode = 500;
				return Content(error, "text/html");
			}
			
			long hits;
            var pgSz = GlobalSettings.GlobalGroupSettings.LookupPageSize;
			model.UserId = string.IsNullOrEmpty(id) ? string.IsNullOrEmpty(model.UserId) ? UserId : model.UserId : id;
			if (model.SearchSortOrder == 0) model.SearchSortOrder = GroupSortOrder.Desc;
			model.PagingSortOrder = model.SearchSortOrder;
			var groups = Flow.Group.GroupsList(ApplicationId, model.UserId, UserId, model.PageNumber, pgSz, model.GroupListType, model.SearchSortBy,model.SearchSortOrder, model.SearchFilter, model.SearchText, out hits);
			var gids = groups.Select(x => x.GroupId).ToArray();
			var isMember = Flow.Group.IsMemberOfGroups(ApplicationId, gids, UserId);

			model.ListOfGroups = groups.Select(x => new GroupsModel
			{
				Group = x,
				IsCurrentUserMember = isMember.Contains(x.GroupId)
			}).ToList();
			 
			model.GroupCount = hits;
			 

			if (model.ListOfGroups.Count > pgSz)
			{
				model.HasNextPage = true;
				model.ListOfGroups.RemoveAt(pgSz);
			}
			model.HasPreviousPage = model.PageNumber != 0;

			var userDetails = Flow.User.GetUserDetails(ApplicationId, model.UserId);

			ViewBag.GroupAvatarUrl = GlobalSettings.GlobalApplicationSettings.DefaultGroupAvatarUrl;
			ViewBag.AvatarUrl = GlobalSettings.GlobalApplicationSettings.AvatarUrl;
			ViewBag.DisplayName = model.UserId != "" + Session["UserId"] ? userDetails.DisplayName : "" + Session["DisplayName"];
			ViewBag.GroupListType = model.UserId != "" + Session["UserId"] ? GroupListType.TheirGroups.LocalizedDesciption() : string.Empty;
			//Set Sort order
			if (model.SearchSortOrder == GroupSortOrder.Asc)
			{
				model.SearchSortOrder = GroupSortOrder.Desc;
			}else if (model.SearchSortOrder == GroupSortOrder.Desc)
			{
				model.SearchSortOrder = GroupSortOrder.Asc;
			}
			if (model.SearchSortOrder == 0) model.SearchSortOrder = GroupSortOrder.Desc;
			
			//set default view type
			model.GroupViewType = userDetails.Settings.DefaultGroupListViewType;
			return PartialView("~/Views/Groups/_GroupList.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
        public virtual ActionResult GroupPeopleList(string id, GroupPeopleListModel model)
		{
			var error = string.Empty;
			if (!string.IsNullOrWhiteSpace(model.SearchText) && (MessageProcessor.HasHtmlContent(model.SearchText, ref error) || MessageProcessor.HasXss(model.SearchText, ref error) || MessageProcessor.HasPds(model.SearchText, ref error)))
			{
				Response.StatusCode = 500;
				return Content(error, "text/html");
			}

			long hits;
            var pgSz = GlobalSettings.GlobalUserSettings.LookupPageSize;
			model.GroupId = string.IsNullOrEmpty(id) ? model.GroupId : id;

			model.ListOfUsers = Flow.Group.GroupPeopleList(ApplicationId, model.GroupId, model.PageNumber, pgSz, model.SearchFilter, model.SearchText ?? string.Empty,PeopleSortOptions.PeopleName,PeopleSortOrder.Asc, out hits);
			model.UserCount = hits;

			model.FollowStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, model.ListOfUsers.Select(x => x.UserId).ToList());

			if (model.ListOfUsers.Count > pgSz)
			{
				model.HasNextPage = true;
				model.ListOfUsers.RemoveAt(pgSz);
			}
			model.HasPreviousPage = model.PageNumber != 0;

			ViewBag.UserAvatarUrl = GlobalSettings.GlobalApplicationSettings.DefaultUserAvatarUrl;
			ViewBag.AvatarUrl = GlobalSettings.GlobalApplicationSettings.AvatarUrl;
			ViewBag.GroupName = Flow.Group.FindGroup(ApplicationId, model.GroupId).GroupName;

			return PartialView("~/Views/Groups/_GroupPeopleList.cshtml", model);
		}

		public virtual ActionResult GroupCreate()
		{
			return PartialView("~/Views/Groups/_CreateGroup.cshtml");
		}

		[HttpGet, PebbleControllerLogging]
		public virtual ActionResult Join(string id)
		{
			if (string.IsNullOrWhiteSpace(id))
				return RedirectToAction("Home", new { groupInviteError = true });

			GroupInvite groupInvite = null;
			try
			{
				groupInvite = Flow.Group.GetPendingGroupInviteByKey(ApplicationId, id);
				if (groupInvite == null)
				{
					return RedirectToAction("Home", new { groupInviteError = true });
				}

				var group = Flow.Group.FindGroup(ApplicationId, groupInvite.GroupId);
				if (group.IsDeleted) return RedirectToAction("Home", new { groupInviteError = true });

				if (groupInvite.UserId != UserId)
				{
					return RedirectToAction("Info", new { id = groupInvite.GroupId, groupInviteIsNotForCurrentUser = true });
				}
				Flow.Group.FollowGroup(ApplicationId, UserId, groupInvite.GroupId);
				Flow.Group.SetGroupInviteStatus(ApplicationId, id, GroupInviteStatuses.Accepted);
				return RedirectToAction("Info", new { id = groupInvite.GroupId });
			}
			catch (Exception)
			{
				if (groupInvite != null) Flow.Group.FollowGroup(ApplicationId, UserId, groupInvite.GroupId);
				Flow.Group.SetGroupInviteStatus(ApplicationId, id, GroupInviteStatuses.YetToAccept);
				return RedirectToAction("Home", new { groupInviteError = true });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult JoinGroup(string groupId)
		{
			try
			{
				Flow.Group.FollowGroup(ApplicationId, UserId, groupId);
				var suggestion = Flow.Suggestions.FindSingleSuggestion(ApplicationId, UserId, groupId, SuggestedType.Group);
				if (suggestion != null)
					Flow.Suggestions.AcceptSuggestion(ApplicationId, UserId, groupId, SuggestedType.Group);

				return Json(new { joinSuccess = true, errorMessage = string.Empty });
			}
			catch (Exception ex)
			{
				return Json(new { joinSuccess = false, errorMessage = ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult LeaveGroup(string groupId)
		{
			try
			{
				//TODO: check to see if admin and remove from groupadmins if not last admin

				Flow.Group.UnFollowGroup(ApplicationId, UserId, groupId);
				return Json(new { leaveSuccess = true, errorMessage = string.Empty });
			}
			catch (Exception ex)
			{
				return Json(new { leaveSuccess = false, errorMessage = ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetCountForSearch(string searchCriteria)
		{
			var filter = (AlphaBetaFilter)Enum.Parse(typeof(AlphaBetaFilter), searchCriteria);
			var groupCount = Flow.Group.GroupListCount(ApplicationId, filter);
			return Json(new { groupcount = groupCount.ToString(CultureInfo.InvariantCulture) });
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult IsGroupIdValidAndAvailable(string groupId)
		{
			bool groupIdValid;
			string groupValidMessage;
			try
			{
				if (!System.Text.RegularExpressions.Regex.IsMatch(groupId, GlobalSettings.GlobalGroupSettings.GroupIdValidationRegEx))
				{
					groupValidMessage = string.Format(Groups.GroupIdNotValid, groupId);

					return Json(new
					{
						groupIdValid = false,
						groupValidMessage,
						groupIdAvailable = false,
						groupIdAvailableMessage = string.Empty
					});
				}

				groupIdValid = Flow.Group.IsValidTagName(groupId);
				groupValidMessage = string.Format(Groups.GroupIdValid, groupId);
			}
			catch (InvalidGroupTagNameLengthException)
			{
				groupIdValid = false;
				groupValidMessage = string.Format(Groups.GroupIdNotValid, groupId);

			}
			catch (GroupIdIsReservedKeywordException)
			{
				groupIdValid = false;
				groupValidMessage = string.Format(Groups.GroupIdCannotBeUsed, groupId);
			}
			catch (Exception)
			{
				groupIdValid = false;
				groupValidMessage = string.Format(Groups.GroupIdNotValid, groupId);
			}

			if (!groupIdValid)
			{
				return Json(new
				{
					groupIdValid = false,
					groupValidMessage,
					groupIdAvailable = false,
					groupIdAvailableMessage = string.Empty
				});
			}
			var groupIdAvailable = Flow.Group.IsTagNameAvailable(ApplicationId, groupId);
			return Json(new
			{
				groupIdValid = true,
				groupValidMessage,
				groupIdAvailable,
				groupIdAvailableMessage = (groupIdAvailable
												? string.Format(Groups.GroupIdAvailable, groupId)
												: string.Format(Groups.GroupIdNotAvailable, groupId))
			});
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult CreateGroup(GroupListModel groupListModel)
		{
			try
			{
				groupListModel.NewGroup.Group.GroupName = groupListModel.NewGroup.Group.GroupName.Trim();
				//groupListModel.NewGroup.Group.TagName = groupListModel.NewGroup.Group.TagName.Trim(); //removing per Jim
				groupListModel.NewGroup.Group.GroupDescription = groupListModel.NewGroup.Group.GroupDescription.Trim();

				if (!System.Text.RegularExpressions.Regex.IsMatch(groupListModel.NewGroup.Group.GroupName, GlobalSettings.GlobalGroupSettings.GroupNameValidationRegEx))
					return Json(new { createSuccess = false, groupId = string.Empty, errorMessage = Groups.GroupNameNotValid });

				var hasAvatar = !string.IsNullOrEmpty(groupListModel.NewGroup.GroupLogo);

				var newGroup = Flow.Group.Create(ApplicationId, UserId, groupListModel.NewGroup.Group.GroupName, groupListModel.NewGroup.Group.AreRestrictedUsersAllowed,
									groupListModel.NewGroup.Group.GroupDescription, string.Empty, groupListModel.NewGroup.Group.IsPublicGroup,
									"", GroupTypes.RegularGroup, hasAvatar);

				if (!string.IsNullOrEmpty(groupListModel.NewGroup.GroupLogo))
					AvatarProcessor.GenerateStandardAvatarImages(
						string.Join(@"\", GlobalSettings.GlobalApplicationSettings.TempFilesLocation, groupListModel.NewGroup.GroupLogo + "_0x0"),
						ApplicationId,
						newGroup.GroupId,
						GlobalSettings.GlobalApplicationSettings.AvatarLocation,
						AvatarTypes.Groups);

				return Json(new { createSuccess = true, groupId = newGroup.GroupId, errorMessage = string.Empty });
			}
			catch (CannotCreateGroupsException)
			{
				return View("PermissionError");
			}
			catch (Exception ex)
			{
				return Json(new { createSuccess = false, groupId = string.Empty, errorMessage = ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult UpdateGroup(GroupInfoModel groupInfoModel)
		{
			try
			{
				groupInfoModel.CurrentGroup.Group.GroupName = groupInfoModel.CurrentGroup.Group.GroupName.Trim();
				//groupInfoModel.CurrentGroup.Group.TagName = groupInfoModel.CurrentGroup.Group.TagName.Trim(); //removing per Jim
				groupInfoModel.CurrentGroup.Group.GroupDescription = groupInfoModel.CurrentGroup.Group.GroupDescription.Trim();

				if (!System.Text.RegularExpressions.Regex.IsMatch(groupInfoModel.CurrentGroup.Group.GroupName,
																	GlobalSettings.GlobalGroupSettings.GroupNameValidationRegEx))
				{
					return Json(new { createSuccess = false, groupId = string.Empty, errorMessage = Groups.GroupNameNotValid });
				}

				if (!string.IsNullOrEmpty(groupInfoModel.CurrentGroup.GroupLogo))
					AvatarProcessor.GenerateStandardAvatarImages(
						string.Join(@"\", GlobalSettings.GlobalApplicationSettings.TempFilesLocation, groupInfoModel.CurrentGroup.GroupLogo + "_0x0"),
						ApplicationId,
						groupInfoModel.CurrentGroup.Group.GroupId,
						GlobalSettings.GlobalApplicationSettings.AvatarLocation,
						AvatarTypes.Groups);

				Flow.Group.UpdateGroup(groupInfoModel.CurrentGroup.Group);
				return Json(new { createSuccess = true, groupId = groupInfoModel.CurrentGroup.Group.GroupId, errorMessage = string.Empty, groupLogo = AvatarProcessor.GetAvatarUrl(ApplicationId, groupInfoModel.CurrentGroup.Group.GroupId, AvatarTypes.Groups, 120, 120) });
			}
			catch (Exception ex)
			{
				return Json(new { createSuccess = false, groupId = string.Empty, errorMessage = ex.Message });
			}
		}

		[PebbleControllerLogging]
		public virtual ActionResult Info(string id, bool? groupInviteIsNotForCurrentUser)
		{
		    var pgSz = GlobalSettings.GlobalUserSettings.LookupSelectionLimit;
			if (id == GlobalSettings.GlobalMessageSettings.AllMessagesGroupId)
				throw new FileNotFoundException();

			groupInviteIsNotForCurrentUser = groupInviteIsNotForCurrentUser ?? false;
			var groupInfoModel = new GroupInfoModel
			{
				CurrentGroup = new GroupModel { Group = Flow.Group.SecureFindGroup(ApplicationId, id, UserId) },
				GroupInviteIsNotForCurrentUser = Convert.ToBoolean(groupInviteIsNotForCurrentUser)
			};

			if (groupInfoModel.CurrentGroup.Group == null || groupInfoModel.CurrentGroup.Group.IsDeleted)
				return View(groupInfoModel);

			groupInfoModel.IsCurrentUserMember = Flow.Group.IsMemberOf(ApplicationId, id, UserId);
			groupInfoModel.IsCurrentUserAdmin = Flow.Group.IsAdminOf(ApplicationId, id, UserId);
			groupInfoModel.CurrentGroup.GroupAction = Resources.Common.Update;
            groupInfoModel.GroupUsers = Flow.Group.UserList(ApplicationId, id, 0, pgSz+1);
			groupInfoModel.FollowStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, groupInfoModel.GroupUsers.Select(x => x.UserId).ToList());
			 
			var userGroups = Flow.Group.GroupListForUser(ApplicationId, UserId);
			groupInfoModel.UserGroups = (userGroups != null && userGroups.Count > 0)
				? userGroups.Select(userGroup => new SelectListItem { Text = userGroup.GroupInformation.GroupName, Value = userGroup.GroupId }).ToList() : new List<SelectListItem>();
			groupInfoModel.LoggedInUserId = UserId;
            ViewBag.MemberPageCount = pgSz;
			ViewBag.UserAvatarUrl = GlobalSettings.GlobalApplicationSettings.DefaultUserAvatarUrl;
			ViewBag.AvatarUrl = GlobalSettings.GlobalApplicationSettings.AvatarUrl;

			return View(groupInfoModel);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult ShowRequestAccessPopUp(string groupId, string groupName)
		{
			var model = new GroupAccessModel { AccessRequestGroupId = groupId, AccessRequestGroupName = groupName };
			return PartialView("_RequestGroupAccess", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult SendGroupAccessRequest(string groupId, string groupAccessRequestReason)
		{
			try
			{
				Flow.Group.RequestAccess(ApplicationId, groupId, UserId, groupAccessRequestReason);
				return Json(new { createSuccess = true, errorMessage = string.Empty });
			}
			catch (GroupAccessRequestReasonIsMissingException ex)
			{
				return Json(new { createSuccess = false, errorMessage = ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult ShowInvitePopUp(string groupId, string groupName)
		{
			var model = new GroupInviteModel { InviteGroupId = groupId, InviteGroupName = groupName, CurrentUserId = UserId };
			return PartialView("_GroupInvite", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetUsersBySearch(string searchText, int userSearchPageNumber, string userSearchPagingAction)
		{
			var model = new GroupInviteModel { PageNumber = Convert.ToInt32(userSearchPageNumber), CurrentUserId = UserId };

			if (userSearchPagingAction.Equals("Init"))
				model.PageNumber = 0;
			if (userSearchPagingAction.Equals("Next"))
				model.PageNumber++;
			if (userSearchPagingAction.Equals("Previous"))
				model.PageNumber--;

            var searchResult = Flow.Search.GetUsers(ApplicationId, searchText, model.PageSize, model.PageNumber, new List<SearchUserColumns> { SearchUserColumns.FullName }, Operator.AND);
		    model.UserList = Flow.User.DetailsOfUsers(ApplicationId, searchResult.ListOfIds);
            model.UserListCount = searchResult.TotalRecords;

			//model.InviteGroupId = groupId;
			//model.InviteGroupName = groupName;

			return PartialView("_UserList", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult SendGroupInviteRequest(string groupId, string inviteeIds)
		{
			try
			{
				Flow.Group.SendGroupInviteToUsers(ApplicationId, groupId, inviteeIds.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList(), UserId);
				return Json(new { inviteSuccess = true, errorMessage = string.Empty });
			}
			catch (Exception ex)
			{
				return Json(new { inviteSuccess = false, errorMessage = ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetGroupUserAndAdminList(string groupId)
		{
			var model = new GroupModel
			{
				GroupAdmins = Flow.Group.ListGroupAdmins(ApplicationId, groupId),
				GroupNonAdminUsers = Flow.Group.UserList(ApplicationId, groupId)
			};

		    if (model.GroupAdmins != null)
		    {
		        model.GroupNonAdminUsers.RemoveAll(x => (model.GroupAdmins.Select(y => y.UserId).Distinct().Contains(x.UserId)));
                model.GroupAdmins.Remove(model.GroupAdmins.Find(x => x.UserId == UserId));
		    }

			return PartialView("_AddRemoveGroupAdmins", model);
		}

		[PebbleControllerLogging]
		public virtual ActionResult GetGroupUserAndAdminsList(string groupId)
		{
			var model = new GroupModel
			{
				GroupAdmins = Flow.Group.ListGroupAdmins(ApplicationId, groupId),
				GroupNonAdminUsers = Flow.Group.UserList(ApplicationId, groupId)
			};

		    if (model.GroupAdmins != null)
		    {
		        model.GroupNonAdminUsers.RemoveAll(x => (model.GroupAdmins.Select(y => y.UserId).Distinct().Contains(x.UserId)));
                model.GroupAdmins.Remove(model.GroupAdmins.Find(x => x.UserId == UserId));
		    }

			return PartialView("_AddRemoveGroupAdmins", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult AddAdminToGroup(string userId, string groupId)
		{
			try
			{
				//TODO: can only admins (or superadmins) add other admins? 

                Flow.Group.AddAdminToGroup(ApplicationId, groupId, userId, UserId);
				var group = Flow.Group.FindGroup(ApplicationId, groupId);
				var admins = string.Join(", ", group.GroupAdmins.Where(x => !x.IsDeleted).Select(x => x.DisplayName).OrderBy(x => x).ToArray());

				return Json(new { addAdminSuccess = true, message = Groups.AdminAddSuccess, admins });
			}
			catch (Exception ex)
			{
				return Json(new { addAdminSuccess = false, message = Groups.AdminAddFailure + " " + ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult RemoveAdminFromGroup(string userId, string groupId)
		{
			try
			{
				//TODO: can only admins (or superadmins) remove other admins? 

				Flow.Group.RemoveAdmin(ApplicationId, userId, groupId, UserId);
				var group = Flow.Group.FindGroup(ApplicationId, groupId);
				var admins = string.Join(", ", group.GroupAdmins.Where(x => !x.IsDeleted).Select(x => x.DisplayName).OrderBy(x => x).ToArray());

				return Json(new { removeAdminSuccess = true, message = Groups.AdminRemoveSuccess, admins });
			}
			catch (Exception ex)
			{
				return Json(new { removeAdminSuccess = false, message = Groups.AdminRemoveFailure + " " + ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetGroupUsers(string groupId)
		{
			var model = new GroupModel
			{
				GroupAdmins = Flow.Group.ListGroupAdmins(ApplicationId, groupId),
				GroupNonAdminUsers = Flow.Group.UserList(ApplicationId, groupId)
			};

			if(model.GroupAdmins != null) model.GroupNonAdminUsers.RemoveAll(x => (model.GroupAdmins.Select(y => y.UserId).Distinct().Contains(x.UserId)));
			model.GroupNonAdminUsers.Remove(model.GroupNonAdminUsers.Find(x => x.UserId == UserId));

			return PartialView("_RemoveMember", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult RemoveUsersFromGroup(string userId, string groupId)
		{
			try
			{
				//TODO: check to see if is the last admin and remove

				Flow.Group.UnFollowGroup(ApplicationId, userId, groupId);
				return Json(new { removeUserSuccess = true, message = Groups.UserRemoveSuccess });
			}
			catch (Exception ex)
			{
				return Json(new { removeUserSuccess = false, message = Groups.UserRemoveFailure + " " + ex.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAccessRequests(string groupId)
		{
			return GetAccessRequestsHelper(groupId);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAccessRequestsCount(string groupId)
		{
			try
			{
				var groupRequests = Flow.Group.FindGroup(ApplicationId, groupId).GroupRequest;

				if (groupRequests != null)
				{
					groupRequests.RemoveAll(x => x.StatusFlag == GroupRequestStatuses.Approved);
					groupRequests.RemoveAll(x => x.StatusFlag == GroupRequestStatuses.Denied);
					return Json(new { groupRequestCount = groupRequests.Count });
				}
			}
			catch (Exception)
			{
				return Json(new { groupRequestCount = "0" });
			}
			return Json(new { groupRequestCount = "0" });
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult ShowRequestRejectCommentsPopUp(string groupId, string rejectUserId)
		{
			var model = new GroupAccessModel
			{
				AccessRequestGroupId = groupId,
				AccessRequestorUserId = rejectUserId
			};
			return PartialView("_RejectAccessRequest", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult UpdateGroupAccessRequest(GroupAccessModel model)
		{
			//TODO: can only admins accept or reject requests?

			switch (model.AccessRequestAction)
			{
				case GroupRequestAction.Accept:
					Flow.Group.UpdateAccessRequest(ApplicationId, model.AccessRequestGroupId,
													model.AccessRequestorUserId, string.Empty,
													GroupRequestStatuses.Approved);
					break;
				case GroupRequestAction.Reject:
					Flow.Group.UpdateAccessRequest(ApplicationId, model.AccessRequestGroupId,
													model.AccessRequestorUserId, model.AccessRejectReason,
													GroupRequestStatuses.Denied);
					break;
			}

			return GetAccessRequestsHelper(model.AccessRequestGroupId);
		}

		// TODO: Why private?
		private ActionResult GetAccessRequestsHelper(string groupId)
		{
			var model = new GroupModel();
			var groupRequests = Flow.Group.FindGroup(ApplicationId, groupId).GroupRequest;
			model.GroupRequests = new List<GroupRequestDetails>();

			if (groupRequests != null)
			{
				foreach (var request in groupRequests)
				{
					if (request.StatusFlag != GroupRequestStatuses.Requested) continue;
					var requestUser = Flow.User.GetUserDetails(ApplicationId, request.UserId);
					model.GroupRequests.Add(new GroupRequestDetails
					{
						UserId = request.UserId,
						RequestorFullName = requestUser.FullName,
						Comments = request.Comments,
						DeclineComments = request.DeclineComments,
						StatusFlag = request.StatusFlag,
						GroupId = groupId
					});
				}
			}

			return PartialView("_AccessRequests", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult DeleteGroup(string groupId)
		{
			try
			{
				Flow.Group.Delete(ApplicationId, groupId, UserId);
				return Json(new { groupDeleteSuccess = true, message = string.Empty });
			}
			catch (UserNotGroupAdminException notAdminEx)
			{
				return Json(new { groupDeleteSuccess = false, message = notAdminEx.Message });
			}
			catch (Exception genericEx)
			{
				return Json(new { groupDeleteSuccess = false, message = genericEx.Message });
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult ShowGroupMessages(string groupId)
		{
			var model = new GroupInfoModel { CurrentGroup = new GroupModel { Group = new Group { GroupId = groupId } } };
			return PartialView("_GroupMessageView", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult SwitchGroupVisibility(GroupInfoModel model)
		{
			try
			{
				//TODO: can only admins switch group type

				if (model.CurrentGroup.Group.IsPublicGroup)
				{
					Flow.Group.SwitchToPrivate(ApplicationId, model.CurrentGroup.Group.GroupId, UserId);
				}
				else
				{
					Flow.Group.SwitchToPublic(ApplicationId, model.CurrentGroup.Group.GroupId, UserId);
				}

				return Json(new { groupSwitchSuccess = true, message = string.Empty });
			}
			catch (Exception genericEx)
			{
				return Json(new { groupSwitchSuccess = false, message = genericEx.Message });
			}
		}

		[HttpGet, PebbleControllerLogging]
		public virtual JsonResult GetUserGroups(string userId)
		{
			var userGroups = Flow.Group.GroupListForUser(ApplicationId, userId);
			var userGroupList = new List<SelectListItem>();

			if (userGroups != null && userGroups.Count > 0)
			{
				userGroups.Sort((x, y) => y.GroupInformation != null ? (x.GroupInformation != null ? String.Compare(x.GroupInformation.GroupName, y.GroupInformation.GroupName, StringComparison.InvariantCultureIgnoreCase) : 0) : 0);
				userGroupList = userGroups.Select(
					userGroup =>
					new SelectListItem { Text = userGroup.GroupInformation.GroupName, Value = userGroup.GroupId })
							.ToList();
			}

			return Json(userGroupList, JsonRequestBehavior.AllowGet);
		}

		[HttpGet, PebbleControllerLogging]
		public virtual JsonResult IsUserMember(string groupId, string userId)
		{
			bool isAlreadyMember;
			try
			{
				isAlreadyMember = Flow.Group.IsMemberOf(ApplicationId, groupId, userId);
			}
			catch (Exception)
			{
				isAlreadyMember = false;
			}
			return Json(isAlreadyMember, JsonRequestBehavior.AllowGet);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult ShowMemberLookup(string groupId)
		{
			var groupUsers = new GroupInfoUsersModel
			{
				GroupUsers = Flow.Group.UserList(ApplicationId, groupId),
                MemberPageCount = GlobalSettings.GlobalUserSettings.LookupSelectionLimit,
				UserAvatarUrl = GlobalSettings.GlobalApplicationSettings.DefaultUserAvatarUrl,
				AvatarUrl = GlobalSettings.GlobalApplicationSettings.AvatarUrl,
				CurrentGroup = new GroupModel { Group = Flow.Group.FindGroup(ApplicationId, groupId) },
				IsCurrentUserAdmin = Flow.Group.IsAdminOf(ApplicationId, groupId, UserId),
				IsCurrentUserMember = Flow.Group.IsMemberOf(ApplicationId, groupId, UserId)
			};

			return PartialView("_GroupMembers", groupUsers);
		}

		public virtual ActionResult MessageFeedGroups()
		{
			var userGroups = Flow.Group.GroupListForUser(ApplicationId, UserId);
			var user = Flow.User.GetUserDetails(ApplicationId, UserId);
			return PartialView("~/Views/Groups/_MyGroupsAsListItems.cshtml", new MyGroupsModel { MyGroups = userGroups, UserSettings = user.Settings });
		}

		[ChildActionOnly]
		public virtual ActionResult JoinViewGroup(GroupViewJoinModel model)
		{
			return PartialView("GroupJoinView", model);
		}

		public virtual ActionResult ExportGroupUsersList(string groupId)
		{
            var model = new GroupMembersModel
			{
                Group = Flow.Group.FindGroup(ApplicationId, groupId),
			};

		    var groupUserStats = Flow.Message.GetGroupUserMessageStats(ApplicationId, groupId);
            var groupUsers = Flow.Group.UserList(ApplicationId, groupId);
		    var members = Flow.User.DetailsOfUsers(ApplicationId, groupUsers.Select(x => x.UserId).ToList());

		    model.GroupUsers =
		            (from m in members
		            join u in groupUserStats
		            on new {m.ApplicationId, m.UserId} equals new {u.ApplicationId, u.UserId} into g
		            from s in g.DefaultIfEmpty()
		            select new User
		            {
		                ApplicationId = m.ApplicationId,
		                UserId = m.UserId,
                        UserName = m.UserName,
		                DisplayName = m.DisplayName,
		                EmployeeInformation = new EmployeeInformation
		                {
		                    Title = m.EmployeeInformation != null ? m.EmployeeInformation.Title : "",
		                    BusinessUnit = m.EmployeeInformation != null ? m.EmployeeInformation.BusinessUnit : "",
		                    Location = m.EmployeeInformation != null ? m.EmployeeInformation.Location : "",
		                },
		                Statistics = new UserStatistics
		                {
		                    MessageCount = s == null ? 0 : s.TotalMessages,
		                    MessageInitiatedCount = s == null ? 0 : s.OriginalPosts,
		                    MessageRespondedCount = s == null ? 0 : s.RepliesInitiated,
		                    LastPostedTime = s == null ? DateTime.MinValue : s.LastPostedOn
		                }
		            }).ToList();

			return View("ExportToExcelGroupMembers", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult SaveUserSettings(string userId,GroupViewType groupViewType)
		{
			 
				var userDetails = Flow.User.GetUserDetails(ApplicationId, userId);
				userDetails.Settings.DefaultGroupListViewType = groupViewType;
				Flow.User.SaveUserSettings(ApplicationId, userId, userDetails.Settings);
				return Json(new { saveUserSettings = true, message = string.Empty });
		}
	}
}