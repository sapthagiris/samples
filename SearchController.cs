﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Pebble.Domain.Flows;
using Pebble.Domain.Models;
using Pebble.Domain.Search;
using Pebble.MainWebSite.Models;
using Pebble.Domain.Exceptions;
using Pebble.MainWebSite.Utils;

namespace Pebble.MainWebSite.Controllers
{
	[SignupCompletionCheck]
	public class SearchController : PebbleController
	{
		public IFlow Flow { get; set; }
		public IMessageContentProcessor MessageContentProcessor { get; set; }

		#region Public Methods
		[HttpGet, PebbleControllerLogging]
		public virtual ActionResult Detail(string simpleSearchCriteria, string detailView)
		{
			var cssError = Flow.Search.IsInvalidSearchInput(simpleSearchCriteria);
			if (!string.IsNullOrWhiteSpace(cssError))
			{
				Response.StatusCode = 500;
				return Content(cssError, "text/html");
			}
			var model = new SearchModel
			{
				SimpleSearchCriteria = simpleSearchCriteria ?? string.Empty,
				IsAdvancedSearch = false,
				IsProfileSearch = false
			};

			try
			{
				model.ResultView = (ResultView)Enum.Parse(typeof(ResultView), detailView);
			}
			catch (Exception)
			{
				model.ResultView = ResultView.Profile;
			}
			model.AdvancedSearch = new AdvancedSearch { ProfileSearch = new ProfileSearch() };

			model = SetAllSearchProfileColumns(model);
			model = GetSearchResultsByView(model);

			return PartialView("~/Views/Search/Detail.cshtml", model);
		}

		[HttpGet]//, PebbleControllerLogging]
		public virtual ActionResult GlobalInstantSearch(string simpleSearchCriteria)
		{
			if (string.IsNullOrEmpty(simpleSearchCriteria))
				return PartialView("~/Views/Search/_ResultsSummary.cshtml", new SearchModel());

			var cssError = Flow.Search.IsInvalidSearchInput(simpleSearchCriteria);
			if (!string.IsNullOrWhiteSpace(cssError))
			{
				Response.StatusCode = 500;
				return Content(cssError, "text/html");
			}

			var model = GetSearchResults(simpleSearchCriteria);
			model.IsAdvancedSearch = false;
			model.IsProfileSearch = false;
			return PartialView("~/Views/Search/_ResultsSummary.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult SearchDetail(string simpleSearchCriteria, string detailView)
		{
			var cssError = Flow.Search.IsInvalidSearchInput(simpleSearchCriteria);
			if (!string.IsNullOrWhiteSpace(cssError))
			{
				Response.StatusCode = 500;
				return Content(cssError, "text/html");
			}

			var model = new SearchModel
			{
				SimpleSearchCriteria = simpleSearchCriteria ?? string.Empty,
				IsAdvancedSearch = false,
				IsProfileSearch = false,
				PageNumber = 0
			};
			try
			{
				model.ResultView = (ResultView)Enum.Parse(typeof(ResultView), detailView);
			}
			catch (Exception)
			{
				model.ResultView = ResultView.Profile;
			}
			model.AdvancedSearch = new AdvancedSearch { ProfileSearch = new ProfileSearch() };
			model = SetAllSearchProfileColumns(model);
			model = GetSearchResultsByView(model);
			return PartialView("~/Views/Search/_ResultsDetail.cshtml", model);
		}

		[HttpGet]
		public virtual ActionResult Advanced(string simpleSearchCriteria)
		{
			var model = new SearchModel { SimpleSearchCriteria = simpleSearchCriteria ?? string.Empty, AdvancedSearch = new AdvancedSearch { SimpleSearchCriteria = simpleSearchCriteria ?? string.Empty, MessagePostedDateFrom = "", MessagePostedDateTo = "" } };
			var thisUser = Flow.User.GetUserDetails(ApplicationId, UserId);
			if (thisUser.Searches != null && thisUser.Searches.Any())
			{
				model.AdvancedSearch.UserSearches = new List<SelectListItem>();
				model.AdvancedSearch.UserSearches.AddRange(thisUser.Searches.Select(search => new SelectListItem
				{
					Text = search.Name,
					Value = search.Name
				}));
			}
			return PartialView("~/Views/Search/Advanced.cshtml", model);
		}

		[HttpGet]
		public virtual ActionResult AdvancedModalSearch(string simpleSearchCriteria)
		{
			var model = new SearchModel
			{
				SimpleSearchCriteria = simpleSearchCriteria ?? string.Empty,
				AdvancedSearch =
					new AdvancedSearch
					{
						SimpleSearchCriteria =
							simpleSearchCriteria ?? string.Empty,
						MessagePostedDateFrom = "",
						MessagePostedDateTo = ""
					},
				IsAdvancedSearch = true
			};
			var thisUser = Flow.User.GetUserDetails(ApplicationId, UserId);
			if (thisUser.Searches != null && thisUser.Searches.Any())
			{
				model.AdvancedSearch.UserSearches = new List<SelectListItem>();
				model.AdvancedSearch.UserSearches.AddRange(thisUser.Searches.Select(search => new SelectListItem
				{
					Text = search.Name,
					Value = search.Name
				}));
			}
			return PartialView("~/Views/Search/Advanced.cshtml", model);
		}

		[HttpPost]
		public virtual ActionResult Advanced(SearchModel model)
		{
			var validationErrors = ValidateSearchModel(model);
			if (!string.IsNullOrEmpty(validationErrors))
			{
				Response.StatusCode = 500;
				return Content(validationErrors, "text/html");
			}

			var thisUser = Flow.User.GetUserDetails(ApplicationId, UserId);
			if (thisUser.Searches != null && thisUser.Searches.Any())
			{
				model.AdvancedSearch.UserSearches = new List<SelectListItem>();
				model.AdvancedSearch.UserSearches.AddRange(thisUser.Searches.Select(search => new SelectListItem
				{
					Text = search.Name,
					Value = search.Name
				}));
			}

			if (model.RedirectToAdvancedSearch)
			{
				return PartialView("~/Views/Search/Advanced.cshtml", model);
			}

			if (model.IsAdvancedSearch)
			{
				model = GetSearchResults(model, false);
				return PartialView("~/Views/Search/_ResultsDetail.cshtml", model);
			}

			if (model.IsProfileSearch)
			{
				model.IsProfileSearch = true;
				model.IsAdvancedSearch = false;
				model = GetProfileResults(model);
				model.ResultView = ResultView.Profile;
				return PartialView("~/Views/Search/Detail.cshtml", model);
			}

			return PartialView("~/Views/Search/Advanced.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAdvancedSearchResults(SearchModel model)
		{
			var validationErrors = ValidateSearchModel(model);
			if (!string.IsNullOrEmpty(validationErrors))
			{
				Response.StatusCode = 500;
				return Content(validationErrors, "text/html");
			}

			model.IsAdvancedSearch = true;
			model.IsProfileSearch = false;
			model = GetSearchResults(model, false);

			return PartialView("~/Views/Search/_ResultsSummary.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetProfileSearchResults(SearchModel model)
		{
			var validationErrors = ValidateSearchModel(model);
			if (!string.IsNullOrEmpty(validationErrors))
			{
				Response.StatusCode = 500;
				return Content(validationErrors, "text/html");
			}

			model.IsProfileSearch = true;
			model.IsAdvancedSearch = false;
			model = GetProfileResults(model);
			model.ResultView = ResultView.Profile;
			return PartialView("~/Views/Search/Detail.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetFilteredProfileSearchResults(SearchModel model)
		{
			var validationErrors = ValidateSearchModel(model);
			if (!string.IsNullOrEmpty(validationErrors))
			{
				Response.StatusCode = 500;
				return Content(validationErrors, "text/html");
			}

			model.IsProfileSearch = true;
			model.IsAdvancedSearch = false;
			model = GetProfileResults(model);
			model.ResultView = ResultView.Profile;
			return PartialView("~/Views/Search/_ResultsDetail.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetSimpleProfileSearchResults(SearchModel model)
		{
			var validationErrors = ValidateSearchModel(model);
			if (!string.IsNullOrEmpty(validationErrors))
			{
				Response.StatusCode = 500;
				return Content(validationErrors, "text/html");
			}

			model.IsProfileSearch = false;
			model.IsAdvancedSearch = false;
			model.ResultView = ResultView.Profile;
			model = GetSearchResultsByView(model);

			return PartialView("~/Views/Search/_ResultsDetail.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult Detail(SearchModel model)
		{
			var validationErrors = ValidateSearchModel(model);
			if (!string.IsNullOrEmpty(validationErrors))
			{
				Response.StatusCode = 500;
				return Content(validationErrors, "text/html");
			}

			var thisUser = Flow.User.GetUserDetails(ApplicationId, UserId);
            if (thisUser.Searches != null && thisUser.Searches.Any())
			{
				if (model.AdvancedSearch != null)
				{
					model.AdvancedSearch.UserSearches = new List<SelectListItem>();
					model.AdvancedSearch.UserSearches.AddRange(thisUser.Searches.Select(search => new SelectListItem
					{
						Text = search.Name,
						Value = search.Name
					}));
				}
			}

			if (model.RedirectToAdvancedSearch)
			{
				return PartialView("~/Views/Search/Advanced.cshtml", model);
			}

			if (model.ResultView == ResultView.Summary)
			{

				model = GetSearchResults(model, false);
				return PartialView("~/Views/Search/_ResultsSummary.cshtml", model);

			}

			if (model.IsAdvancedSearch)
			{
				model = GetSearchResults(model, true);
			}
			else if (model.IsProfileSearch)
			{
				model = GetProfileResults(model);
			}
			model.ResultView = model.ResultView;

			return PartialView("~/Views/Search/Detail.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult SaveUserSearch(SearchModel model)
		{
			try
			{
				var validationErrors = ValidateSearchModel(model);
				if (!string.IsNullOrEmpty(validationErrors))
				{
					Response.StatusCode = 500;
					return Content(validationErrors, "text/html");
				}

				Flow.User.AddUserSearch(ApplicationId, UserId,
										new UserSearch
										{
											Name = string.IsNullOrWhiteSpace(model.AdvancedSearch.SearchName) ? string.Empty : model.AdvancedSearch.SearchName.Trim(),
											BusinessUnitText = string.IsNullOrWhiteSpace(model.AdvancedSearch.BusinessUnit) ? string.Empty : model.AdvancedSearch.BusinessUnit.Trim(),
											DepartmentText = string.IsNullOrWhiteSpace(model.AdvancedSearch.Department) ? string.Empty : model.AdvancedSearch.Department.Trim(),
											GroupText = string.IsNullOrWhiteSpace(model.AdvancedSearch.GroupNameOrDesc) ? string.Empty : model.AdvancedSearch.GroupNameOrDesc.Trim(),
											HashTagText = string.IsNullOrWhiteSpace(model.AdvancedSearch.HashTag) ? string.Empty : model.AdvancedSearch.HashTag.Trim(),
											LocationText = string.IsNullOrWhiteSpace(model.AdvancedSearch.Location) ? string.Empty : model.AdvancedSearch.Location.Trim(),
											MemberText = string.IsNullOrWhiteSpace(model.AdvancedSearch.MemberName) ? string.Empty : model.AdvancedSearch.MemberName.Trim(),
											MessageText = string.IsNullOrWhiteSpace(model.AdvancedSearch.MessageText) ? string.Empty : model.AdvancedSearch.MessageText.Trim(),
											MessagePostedDateFrom = string.IsNullOrWhiteSpace(model.AdvancedSearch.MessagePostedDateFrom) ? string.Empty : model.AdvancedSearch.MessagePostedDateFrom.Trim(),
											MessagePostedDateTo = string.IsNullOrWhiteSpace(model.AdvancedSearch.MessagePostedDateTo) ? string.Empty : model.AdvancedSearch.MessagePostedDateTo.Trim()
										});
				return Json(new { saveSearchSuccess = true, message = Resources.Search.SaveSearchSuccessMessage });
			}
			catch (SearchNameAlreadyExistsException snaeex)
			{
				return Json(new { saveSearchSuccess = false, message = snaeex.Message });
			}
			catch (Exception ex)
			{
				return Json(new { saveSearchSuccess = false, message = ex.Message });
			}

		}

		[HttpGet, PebbleControllerLogging]
		public virtual JsonResult GetUserSearch(string searchName)
		{
			try
			{

				var user = Flow.User.GetUserDetails(ApplicationId, UserId);
				var search = user.Searches.FirstOrDefault(s => s.Name.Equals(searchName));
				if (search != null)
					return Json(new
					{
						getSearchSuccess = true,
						errorMessage = string.Empty,
						search.MemberText,
						search.LocationText,
						search.GroupText,
						search.BusinessUnitText,
						search.HashTagText,
						search.DepartmentText,
						search.MessageText,
						search.MessagePostedDateFrom,
						search.MessagePostedDateTo
					}, JsonRequestBehavior.AllowGet);

				return Json(new { getSearchSuccess = false, errorMessage = string.Format(Resources.Search.NoSearchFound, searchName) }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(new { getSearchSuccess = false, errorMessage = ex.Message }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetGroupMessages(string groupId)
		{
			var model = new GroupSearchItem();
			try
			{
				var group = Flow.Group.FindGroup(ApplicationId, groupId);
				if (!@group.IsPublicGroup)
				{
					if (!Flow.Group.IsMemberOf(ApplicationId, groupId, UserId))
					{
						throw new Exception(Resources.Search.NotAuthorizedToSeeMessages);
					}
				}

				var messages = Flow.Message.MessageList(ApplicationId, UserId, groupId,
																PseudoGroupType.None, 2, 0, string.Empty,
																MessageSortCoulmn.OriginalMessageDate,
																MessageThreadSort.EachMessageSepartely,
																string.Empty, false, false);
				if (messages != null)
				{
					model.GroupMessages = messages.Select(message => new MessageSearchItem
					{
						GroupsPostedTo = message.GroupsPostedTo,
						AllMessagesGroupId = GlobalSettings.GlobalMessageSettings.AllMessagesGroupId,
						Message = message.MessageContent,
						MessageId = message.MessageId,
						MessageUserFullName = message.UserInformation.DisplayName,
						MessageUserJobTitle = message.UserInformation.Title,
						UserId = message.UserInformation.UserId,
						PostedTime = message.PostedTime
					}).ToList();
				}

			}
			catch (Exception ex)
			{
				model.GroupMessages = null;
				model.GetMessagesResult = ex.Message;
			}

			return PartialView("~/Views/Search/_GroupMessages.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAdditionalSearchResults(SearchModel model)
		{
			if (model.IsAdvancedSearch)
				return GetAdditionalResultsAdvanced(model);

			if (model.IsProfileSearch)
				return GetAdditionalProfileResults(model);

			return GetAdditionalResults(model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAdditionalResults(SearchModel model)
		{
			try
			{
				model.ResultView = (ResultView)Enum.Parse(typeof(ResultView), model.ResultView.ToString());
			}
			catch (Exception)
			{
				model.ResultView = ResultView.Members;
			}
			//model.PageNumber += 1;

			GetSearchResultsByView(model);

			return PartialView("~/Views/Search/_AdditionalSearchResults.cshtml", model);
		}

		public virtual ActionResult GetAdditionalProfileResults(SearchModel model)
		{
			model.ResultView = ResultView.Profile;
			return GetAdditionalResultsAdvanced(model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAdditionalResultsAdvanced(SearchModel model)
		{

			var pgSz = GlobalSettings.GlobalSearchSettings.SearchPagingRecordSize;
			var pgSzNextRecord = pgSz - 1;
			switch (model.ResultView)
			{
				case ResultView.Messages:

                    var fromDate = !string.IsNullOrEmpty(model.AdvancedSearch.MessagePostedDateFrom) ? DateTime.Parse(model.AdvancedSearch.MessagePostedDateFrom).ToString("yyyy-MM-dd") : string.Empty;
                    var toDate = !string.IsNullOrEmpty(model.AdvancedSearch.MessagePostedDateTo) ? DateTime.Parse(model.AdvancedSearch.MessagePostedDateTo).ToString("yyyy-MM-dd") : string.Empty;
					var searchResult = Flow.Search.GetMessages(ApplicationId, model.AdvancedSearch.MessageText,
																model.AdvancedSearch.HashTag, model.AdvancedSearch.MemberName, model.AdvancedSearch.GroupNameOrDesc,
																fromDate, toDate, pgSz, model.PageNumber, UserId,
																GetPrivateGroups(), HasPermission(PermissionType.RestrictedUser));
					model.Messages = Flow.Message.FindMessages(ApplicationId, searchResult.ListOfIds).OrderByDescending(x => x.ThreadUpdatedTime).Select(message => new MessageSearchItem
					{
						GroupsPostedTo = message.GroupsPostedTo,
						AllMessagesGroupId = GlobalSettings.GlobalMessageSettings.AllMessagesGroupId,
						Message = message.MessageContent,
						MessageId = message.MessageId,
						MessageUserFullName = message.UserInformation.DisplayName,
						MessageUserJobTitle = message.UserInformation.Title,
						UserId = message.UserInformation.UserId,
						PostedTime = message.PostedTime
					}).ToList();
					model.MessageCount = searchResult.TotalRecords;
					if (model.Messages != null && model.Messages.Count > pgSzNextRecord)
					{
						model.HasMoreMessages = true;
						model.Messages.RemoveAt(pgSzNextRecord);
					}
					break;
				case ResultView.Groups:
					var groupSearchResult = Flow.Search.GetGroups(ApplicationId, model.AdvancedSearch.GroupNameOrDesc, pgSz, model.PageNumber, HasPermission(PermissionType.RestrictedUser));
					var groupMembership = Flow.Group.IsMemberOfGroups(ApplicationId, groupSearchResult.ListOfIds.ToArray(), UserId);
		            var groups = Flow.Group.FindGroups(ApplicationId, groupSearchResult.ListOfIds);
                    model.Groups = new List<GroupSearchItem>();
                    foreach (var @group in groupSearchResult.ListOfIds.Select(gid => groups.FirstOrDefault(x => x.GroupId == gid)).Where(@group => @group != null))
                    {
                        model.Groups.Add(new GroupSearchItem
                        {
                            GroupItem = @group,
                            IsCurrentUserMember = groupMembership.Contains(@group.GroupId),
                            UserId = UserId
                        });
                    }
					model.GroupCount = groupSearchResult.TotalRecords;

					if (model.Groups != null && model.Groups.Count > pgSzNextRecord)
					{
						model.HasMoreGroups = true;
						model.Groups.RemoveAt(pgSzNextRecord);
					}
					break;
				case ResultView.Profile:
					var searchColumns = new List<SearchUserColumns>();
					SearchResult userSearchResult = null;
					if (model.IsAdvancedSearch)
					{
						var searchColumnsAdv = new Dictionary<SearchUserColumns, string>();
						if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.MemberName))
							searchColumnsAdv.Add(SearchUserColumns.DisplayName, model.AdvancedSearch.MemberName);
						if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.Location))
							searchColumnsAdv.Add(SearchUserColumns.Location, model.AdvancedSearch.Location);
						if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.Department))
							searchColumnsAdv.Add(SearchUserColumns.Department, model.AdvancedSearch.Department);
						if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.BusinessUnit))
							searchColumnsAdv.Add(SearchUserColumns.BusinessUnit, model.AdvancedSearch.BusinessUnit);

						userSearchResult = Flow.Search.GetUsers(ApplicationId, 30, model.PageNumber, searchColumnsAdv);
					}
					else if (model.IsProfileSearch)
					{
						searchColumns = GetProfileSearchColumns(model);
						userSearchResult = Flow.Search.GetUsers(ApplicationId, model.AdvancedSearch.ProfileSearch.ProfileSearchText, pgSz, model.PageNumber, searchColumns);
					}
					model.Profiles = Flow.User.DetailsOfUsers(ApplicationId, userSearchResult.ListOfIds)
										 .Select(user => new MemberSearchItem
										 {
											 MemberItem = user,
											 ShowProfileContent = true,
											 ProfileHits = GetFieldsWithHits(Flow.Search.GetSearchUser(user), model.IsAdvancedSearch ? string.Join(" ", model.AdvancedSearch.Location, model.AdvancedSearch.Department, model.AdvancedSearch.BusinessUnit) : model.AdvancedSearch.ProfileSearch.ProfileSearchText, model.IsAdvancedSearch, searchColumns),
											 FollowStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, user.UserId)
										 }).ToList();
					model.Profiles.Sort((x, y) => y.ProfileHits != null ? (x.ProfileHits != null ? (x.ProfileHits.Count <= y.ProfileHits.Count ? 1 : -1) : 0) : 0);

					model.ProfileCount = userSearchResult.TotalRecords;
					model.ProfileCriteriaHits = Flow.Search.GetProfileCriteriaCounts(ApplicationId, searchColumns, model.AdvancedSearch.ProfileSearch.ProfileSearchText);
					if (model.Profiles != null && model.Profiles.Count > pgSzNextRecord)
					{
						model.HasMoreProfiles = true;
						model.Profiles.RemoveAt(pgSzNextRecord);
					}
					break;


			}

			return PartialView("~/Views/Search/_AdditionalSearchResults.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult GetAdvancedSearchDetailResults(SearchModel model)
		{
			model = GetSearchResults(model, true);
			return PartialView("~/Views/Search/_ResultsDetail.cshtml", model);
		}

		[HttpPost, PebbleControllerLogging]
		public virtual ActionResult MoreProfileInfoCard(string userId, string simpleSearchCriteria)
		{
			var peopleIFollow = Flow.Follower.PeopleIAmFollowing(ApplicationId, UserId).Select(item => item.UserId);
			var myGroups = Flow.Group.GroupListForUser(ApplicationId, UserId).Select(item => item.GroupId);
			var user = Flow.User.GetUserDetails(ApplicationId, userId);

			var model = new MemberSearchItem
			{
				MemberItem = user,
				UserId = userId,
				CommonGroups = Flow.Group.GroupListForUser(ApplicationId, userId).Select(item => item.GroupId).Intersect(myGroups).Count(),
				FollowStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, userId),
				MutualConnections = Flow.Follower.PeopleIAmFollowing(ApplicationId, user.UserId).Select(item => item.UserId).Intersect(peopleIFollow).Count(),
				ProfileHits = GetFieldsWithHits(Flow.Search.GetSearchUser(user), simpleSearchCriteria, false, new List<SearchUserColumns> { SearchUserColumns.All }),
				ShowProfileContent = true
			};

			return PartialView("MoreProfileInfoCard", model);
		}
		#endregion


		#region Private Methods
		private string ValidateSearchModel(SearchModel model)
		{
			var cssError = string.Empty;
			if (!string.IsNullOrEmpty(model.SimpleSearchCriteria))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.SimpleSearchCriteria);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.SearchName))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.SearchName);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.SimpleSearchCriteria))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.SimpleSearchCriteria);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.MemberName))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.MemberName);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.Location))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.Location);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.MessageText))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.MessageText);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.BusinessUnit))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.BusinessUnit);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.GroupNameOrDesc))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.GroupNameOrDesc);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.Department))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.Department);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.HashTag))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.HashTag);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.MessagePostedDateFrom))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.MessagePostedDateFrom);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.MessagePostedDateTo))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.MessagePostedDateTo);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (model.AdvancedSearch != null && model.AdvancedSearch.ProfileSearch != null && !string.IsNullOrEmpty(model.AdvancedSearch.ProfileSearch.ProfileSearchText))
			{
				cssError = Flow.Search.IsInvalidSearchInput(model.AdvancedSearch.ProfileSearch.ProfileSearchText);
				Response.StatusCode = 500;
				if (!string.IsNullOrEmpty(cssError)) return cssError;
			}
			if (string.IsNullOrEmpty(cssError)) Response.StatusCode = 200;
			return cssError;
		}

		private SearchModel GetSearchResults(SearchModel model, bool isDetailView)
		{
			var pgSz = GlobalSettings.GlobalSearchSettings.SearchPagingRecordSize;
			var pgSzNextRecord = pgSz - 1;

			var searchSummaryRecordCount = GlobalSettings.GlobalSearchSettings.SearchSummaryRecordCount;

			if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.MessageText) || !string.IsNullOrWhiteSpace(model.AdvancedSearch.HashTag)
				|| !string.IsNullOrWhiteSpace(model.AdvancedSearch.MemberName) || !string.IsNullOrWhiteSpace(model.AdvancedSearch.GroupNameOrDesc))
			{
				var fromDate = DateTime.Parse(model.AdvancedSearch.MessagePostedDateFrom ?? DateTime.MinValue.ToShortDateString());
				var toDate = DateTime.Parse(model.AdvancedSearch.MessagePostedDateTo ?? DateTime.MinValue.ToShortDateString());
				var searchResult = Flow.Search.GetMessages(ApplicationId, model.AdvancedSearch.MessageText,
															model.AdvancedSearch.HashTag, model.AdvancedSearch.MemberName, model.AdvancedSearch.GroupNameOrDesc,
															fromDate.ToString("yyyy-MM-dd"),
															toDate.ToString("yyyy-MM-dd"), isDetailView ? pgSz : searchSummaryRecordCount, model.PageNumber, UserId,
															GetPrivateGroups(), HasPermission(PermissionType.RestrictedUser));
				model.Messages = Flow.Message.FindMessages(ApplicationId, searchResult.ListOfIds).OrderByDescending(x => x.ThreadUpdatedTime).Select(message => new MessageSearchItem
				{
					GroupsPostedTo = message.GroupsPostedTo,
					AllMessagesGroupId = GlobalSettings.GlobalMessageSettings.AllMessagesGroupId,
					Message = MessageContentProcessor.HighlightMessageLinks(ApplicationId,message.MessageContent,false,false),
					MessageId = message.MessageId,
					MessageUserFullName = message.UserInformation.DisplayName,
					MessageUserJobTitle = message.UserInformation.Title,
					UserId = message.UserInformation.UserId,
					PostedTime = message.PostedTime
				}).ToList();
				model.MessageCount = searchResult.TotalRecords;

				if (model.Messages != null && model.Messages.Count > pgSzNextRecord)
				{
					model.HasMoreMessages = true;
					model.Messages.RemoveAt(pgSzNextRecord);
				}
			}

			if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.GroupNameOrDesc))
			{
				var groupSearchResult = Flow.Search.GetGroups(ApplicationId, model.AdvancedSearch.GroupNameOrDesc, isDetailView ? pgSz : searchSummaryRecordCount, model.PageNumber, HasPermission(PermissionType.RestrictedUser));
                var groupMembership = Flow.Group.IsMemberOfGroups(ApplicationId, groupSearchResult.ListOfIds.ToArray(), UserId);
                var groups = Flow.Group.FindGroups(ApplicationId, groupSearchResult.ListOfIds);
                model.Groups = new List<GroupSearchItem>();
                foreach (var @group in groupSearchResult.ListOfIds.Select(gid => groups.FirstOrDefault(x => x.GroupId == gid)).Where(@group => @group != null))
                {
                    model.Groups.Add(new GroupSearchItem
                    {
                        GroupItem = @group,
                        IsCurrentUserMember = groupMembership.Contains(@group.GroupId),
                        UserId = UserId
                    });
                }
				model.GroupCount = groupSearchResult.TotalRecords;
			}
			if (model.Groups != null && model.Groups.Count > pgSzNextRecord)
			{
				model.HasMoreGroups = true;
				model.Groups.RemoveAt(pgSzNextRecord);
			}
			//IEnumerable<string> peopleIFollow = null;
			//IEnumerable<string> myGroups = null;
			if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.MemberName))
			{
				//var searchColumns = new List<SearchUserColumns> { SearchUserColumns.FullName, SearchUserColumns.DisplayName };

				//var userSearchResult = Flow.Search.GetUsers(ApplicationId, model.AdvancedSearch.MemberName, isDetailView ? pgSz : searchSummaryRecordCount, model.PageNumber, searchColumns);
				//peopleIFollow = Flow.Follower.PeopleIAmFollowing(ApplicationId, UserId).Select(item => item.UserId);
				//myGroups = Flow.Group.GroupListForUser(ApplicationId, UserId).Select(item => item.GroupId);
				//var followStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, userSearchResult.ListOfIds);
				/*
				model.Users = Flow.User.DetailsOfUsers(ApplicationId, userSearchResult.ListOfIds)
									 .Select(user => new MemberSearchItem
									 {
										 MemberItem = user,
										 MutualConnections = Flow.Follower.PeopleIAmFollowing(ApplicationId, user.UserId).Select(item => item.UserId).Intersect(peopleIFollow).Count(),
										 FollowStatus = followStatus[user.UserId],
										 CommonGroups = Flow.Group.GroupListForUser(ApplicationId, user.UserId).Select(item => item.GroupId).Intersect(myGroups).Count()
									 }).ToList();

				model.UserCount = userSearchResult.TotalRecords;
				*/
			}

			if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.MemberName) ||
			!string.IsNullOrWhiteSpace(model.AdvancedSearch.Location) ||
			 !string.IsNullOrWhiteSpace(model.AdvancedSearch.Department) ||
			 !string.IsNullOrWhiteSpace(model.AdvancedSearch.BusinessUnit))
			{

				var searchColumns = new Dictionary<SearchUserColumns, string>();
				if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.MemberName))
				{
					searchColumns.Add(SearchUserColumns.DisplayName, model.AdvancedSearch.MemberName);
				}
				if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.Location))
				{
					searchColumns.Add(SearchUserColumns.Location, model.AdvancedSearch.Location);
				}
				if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.Department))
				{
					searchColumns.Add(SearchUserColumns.Department, model.AdvancedSearch.Department);

				}
				if (!string.IsNullOrWhiteSpace(model.AdvancedSearch.BusinessUnit))
				{
					searchColumns.Add(SearchUserColumns.BusinessUnit, model.AdvancedSearch.BusinessUnit);
				}

				var userSearchResult = Flow.Search.GetUsers(ApplicationId, isDetailView ? 30 : 6, model.PageNumber, searchColumns);
				//if (peopleIFollow == null) peopleIFollow = Flow.Follower.PeopleIAmFollowing(ApplicationId, UserId).Select(item => item.UserId);
				//if (myGroups == null) myGroups = Flow.Group.GroupListForUser(ApplicationId, UserId).Select(item => item.GroupId);
				var followStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, userSearchResult.ListOfIds);
				model.Profiles = Flow.User.DetailsOfUsers(ApplicationId, userSearchResult.ListOfIds)
									 .Select(user => new MemberSearchItem
									 {
										 MemberItem = user,
										 ShowProfileContent = true,
										 ProfileHits = GetFieldsWithHits(Flow.Search.GetSearchUser(user), string.Join(" ", model.AdvancedSearch.MemberName ?? string.Empty, model.AdvancedSearch.Location ?? string.Empty, model.AdvancedSearch.Department ?? string.Empty, model.AdvancedSearch.BusinessUnit ?? string.Empty), true, searchColumns.Keys.ToList()),
										 FollowStatus = followStatus[user.UserId],
										 // MutualConnections = Flow.Follower.PeopleIAmFollowing(ApplicationId, user.UserId).Select(item => item.UserId).Intersect(peopleIFollow).Count(),
										 // CommonGroups = Flow.Group.GroupListForUser(ApplicationId, user.UserId).Select(item => item.GroupId).Intersect(myGroups).Count()
									 }).ToList();
				model.Profiles.Sort((x, y) => y.ProfileHits != null ? (x.ProfileHits != null ? (x.ProfileHits.Count <= y.ProfileHits.Count ? 1 : -1) : 0) : 0);
				model.ProfileCount = userSearchResult.TotalRecords;
				if (model.Profiles != null && model.Profiles.Count > pgSzNextRecord)
				{
					model.HasMoreProfiles = true;
					model.Profiles.RemoveAt(pgSzNextRecord);
				}
			}

			return model;
		}

		private SearchModel GetProfileResults(SearchModel model)
		{
			var searchColumns = GetProfileSearchColumns(model);
			var pgSz = GlobalSettings.GlobalSearchSettings.SearchPagingRecordSize;
			var pgSzNextRecord = pgSz - 1;
			if (searchColumns.Count > 0)
			{
				var userSearchResult = Flow.Search.GetUsers(ApplicationId, model.IsProfileSearch ? model.AdvancedSearch.ProfileSearch.ProfileSearchText : model.SimpleSearchCriteria, pgSz, model.PageNumber, searchColumns);
				//var peopleIFollow = Flow.Follower.PeopleIAmFollowing(ApplicationId, UserId).Select(item => item.UserId);
				//var myGroups = Flow.Group.GroupListForUser(ApplicationId, UserId).Select(item => item.GroupId);
				var followStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, userSearchResult.ListOfIds);
				model.Profiles = Flow.User.DetailsOfUsers(ApplicationId, userSearchResult.ListOfIds)
									 .Select(user => new MemberSearchItem
									 {
										 MemberItem = user,
										 ShowProfileContent = true,
										 ProfileHits = GetFieldsWithHits(Flow.Search.GetSearchUser(user), model.IsProfileSearch ? model.AdvancedSearch.ProfileSearch.ProfileSearchText : model.SimpleSearchCriteria, false, searchColumns),
										 FollowStatus = followStatus[user.UserId],
										 //MutualConnections = Flow.Follower.PeopleIAmFollowing(ApplicationId, user.UserId).Select(item => item.UserId).Intersect(peopleIFollow).Count(),
										 //CommonGroups = Flow.Group.GroupListForUser(ApplicationId, user.UserId).Select(item => item.GroupId).Intersect(myGroups).Count()
									 }).ToList();
				model.Profiles.Sort((x, y) => y.ProfileHits != null ? (x.ProfileHits != null ? (x.ProfileHits.Count <= y.ProfileHits.Count ? 1 : -1) : 0) : 0);
				model.ProfileCount = userSearchResult.TotalRecords;
				model.ProfileCriteriaHits = Flow.Search.GetProfileCriteriaCounts(ApplicationId, searchColumns,
																				 model.IsProfileSearch ? model.AdvancedSearch.ProfileSearch.ProfileSearchText : model.SimpleSearchCriteria);
				if (model.Profiles != null && model.Profiles.Count > pgSzNextRecord)
				{
					model.HasMoreProfiles = true;
					model.Profiles.RemoveAt(pgSzNextRecord);
				}
			}

			return model;
		}

		private SearchModel GetSearchResults(string simpleSearchCriteria)
		{
			var searchSummaryRecordCount = GlobalSettings.GlobalSearchSettings.SearchSummaryRecordCount;
			var model = new SearchModel { SimpleSearchCriteria = simpleSearchCriteria ?? string.Empty };

			var searchResult = Flow.Search.GetMessages(ApplicationId, simpleSearchCriteria, searchSummaryRecordCount, 0, UserId, GetPrivateGroups(), HasPermission(PermissionType.RestrictedUser));

			model.Messages = Flow.Message.FindMessages(ApplicationId, searchResult.ListOfIds).OrderByDescending(x => x.ThreadUpdatedTime).Select(message => new MessageSearchItem
			{
				GroupsPostedTo = message.GroupsPostedTo,
				AllMessagesGroupId = GlobalSettings.GlobalMessageSettings.AllMessagesGroupId,
				Message =  MessageContentProcessor.HighlightMessageLinks(ApplicationId,message.MessageContent,false,false),
				MessageId = message.MessageId,
				MessageUserJobTitle = message.UserInformation.Title,
				MessageUserFullName = message.UserInformation.DisplayName,
				UserId = message.UserInformation.UserId,
				PostedTime = message.PostedTime,
			}).ToList();
			model.MessageCount = searchResult.TotalRecords;

			var groupSearchResult = Flow.Search.GetGroups(ApplicationId, simpleSearchCriteria, searchSummaryRecordCount, 0, HasPermission(PermissionType.RestrictedUser));
			var groupMembership = Flow.Group.IsMemberOfGroups(ApplicationId, groupSearchResult.ListOfIds.ToArray(), UserId);
		    var groups = Flow.Group.FindGroups(ApplicationId, groupSearchResult.ListOfIds);
            model.Groups = new List<GroupSearchItem>();
            foreach (var @group in groupSearchResult.ListOfIds.Select(gid => groups.FirstOrDefault(x => x.GroupId == gid)).Where(@group => @group != null))
            {
                model.Groups.Add(new GroupSearchItem
                {
                    GroupItem = @group,
                    IsCurrentUserMember = groupMembership.Contains(@group.GroupId),
                    UserId = UserId
                });
            }
			model.GroupCount = groupSearchResult.TotalRecords;

			var searchColumns = new List<SearchUserColumns> { SearchUserColumns.All };

			var userSearchResult = Flow.Search.GetUsers(ApplicationId, simpleSearchCriteria, searchSummaryRecordCount, 0, searchColumns);
			//var peopleIFollow = Flow.Follower.PeopleIAmFollowing(ApplicationId, UserId).Select(item => item.UserId);
			//var myGroups = Flow.Group.GroupListForUser(ApplicationId, UserId).Select(item => item.GroupId);
			var followStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, userSearchResult.ListOfIds);
			model.Profiles = Flow.User.DetailsOfUsers(ApplicationId, userSearchResult.ListOfIds)
								 .Select(user => new MemberSearchItem
								 {
									 MemberItem = user,
									 ShowProfileContent = true,
									 ProfileHits = GetFieldsWithHits(Flow.Search.GetSearchUser(user), simpleSearchCriteria, false, searchColumns),
									 //MutualConnections = Flow.Follower.PeopleIAmFollowing(ApplicationId, user.UserId).Select(item => item.UserId).Intersect(peopleIFollow).Count(),
									 FollowStatus = followStatus[user.UserId],
									 //CommonGroups = Flow.Group.GroupListForUser(ApplicationId, user.UserId).Select(item => item.GroupId).Intersect(myGroups).Count()
								 }).ToList();

			model.Profiles.Sort((x, y) => y.ProfileHits != null ? (x.ProfileHits != null ? (x.ProfileHits.Count <= y.ProfileHits.Count ? 1 : -1) : 0) : 0);
			model.ProfileCount = userSearchResult.TotalRecords;

			return model;
		}

		private SearchModel GetSearchResultsByView(SearchModel model)
		{
			var pgSz = GlobalSettings.GlobalSearchSettings.SearchPagingRecordSize;
			var pgSzNextRecord = pgSz - 1;
			var searchResult = Flow.Search.GetMessages(ApplicationId, model.SimpleSearchCriteria, pgSz, model.PageNumber, UserId, GetPrivateGroups(), HasPermission(PermissionType.RestrictedUser));
			if (model.ResultView == ResultView.Messages)
			{
				model.Messages = Flow.Message.FindMessages(ApplicationId, searchResult.ListOfIds).OrderByDescending(x => x.ThreadUpdatedTime).Select(message => new MessageSearchItem
				{
					GroupsPostedTo = message.GroupsPostedTo,
					AllMessagesGroupId = GlobalSettings.GlobalMessageSettings.AllMessagesGroupId,
					Message =  MessageContentProcessor.HighlightMessageLinks(ApplicationId,message.MessageContent,false,false),
					MessageId = message.MessageId,
					MessageUserFullName = message.UserInformation.DisplayName,
					MessageUserJobTitle = message.UserInformation.Title,
					UserId = message.UserInformation.UserId,
					PostedTime = message.PostedTime
				}).ToList();
			}
			model.MessageCount = searchResult.TotalRecords;
			if (model.Messages != null && model.Messages.Count > pgSzNextRecord)
			{
				model.HasMoreMessages = true;
				model.Messages.RemoveAt(pgSzNextRecord);
			}

			var groupSearchResult = Flow.Search.GetGroups(ApplicationId, model.SimpleSearchCriteria, pgSz, model.PageNumber, HasPermission(PermissionType.RestrictedUser));
			if (model.ResultView == ResultView.Groups)
			{
                var groupMembership = Flow.Group.IsMemberOfGroups(ApplicationId, groupSearchResult.ListOfIds.ToArray(), UserId);
			    var groups = Flow.Group.FindGroups(ApplicationId, groupSearchResult.ListOfIds);
                model.Groups = new List<GroupSearchItem>();
                foreach (var @group in groupSearchResult.ListOfIds.Select(gid => groups.FirstOrDefault(x => x.GroupId == gid)).Where(@group => @group != null))
                {
                    model.Groups.Add(new GroupSearchItem
                    {
                        GroupItem = @group,
                        IsCurrentUserMember = groupMembership.Contains(@group.GroupId),
                        UserId = UserId
                    });
                }
			}
			model.GroupCount = groupSearchResult.TotalRecords;

			if (model.Groups != null && model.Groups.Count > pgSzNextRecord)
			{
				model.HasMoreGroups = true;
				model.Groups.RemoveAt(pgSzNextRecord);
			}
			var searchColumns = new List<SearchUserColumns>();
			if (!model.IsAdvancedSearch || !model.IsProfileSearch)
				searchColumns.Add(SearchUserColumns.All);
			else
				searchColumns = GetProfileSearchColumns(model);

			var userSearchResult = Flow.Search.GetUsers(ApplicationId, model.SimpleSearchCriteria, pgSz, model.PageNumber, searchColumns);

			if (model.ResultView == ResultView.Profile)
			{

				if (model.AdvancedSearch != null && model.AdvancedSearch.ProfileSearch != null)
					model.AdvancedSearch.ProfileSearch.ProfileSearchText = model.SimpleSearchCriteria;

				model.Profiles = Flow.User.DetailsOfUsers(ApplicationId, userSearchResult.ListOfIds)
									.Select(user => new MemberSearchItem
									{
										MemberItem = user,
										ShowProfileContent = true,
										ProfileHits = GetFieldsWithHits(Flow.Search.GetSearchUser(user), model.SimpleSearchCriteria, false, searchColumns),
										FollowStatus = Flow.Follower.GetFollowStatus(ApplicationId, UserId, user.UserId)
									}).ToList();
				model.ProfileCriteriaHits = Flow.Search.GetProfileCriteriaCounts(ApplicationId, searchColumns, model.SimpleSearchCriteria);
				model.Profiles.Sort((x, y) => y.ProfileHits != null ? (x.ProfileHits != null ? (x.ProfileHits.Count <= y.ProfileHits.Count ? 1 : -1) : 0) : 0);
			}

			model.ProfileCount = userSearchResult.TotalRecords;
			if (model.Profiles != null && model.Profiles.Count > pgSzNextRecord)
			{
				model.HasMoreProfiles = true;
				model.Profiles.RemoveAt(pgSzNextRecord);
			}
			return model;
		}

		private static Dictionary<string, string> GetFieldsWithHits(SearchUser user, string searchCriteria, bool advancedSearch, List<SearchUserColumns> columns)
		{
			var hitFields = new Dictionary<string, string>();
			if (string.IsNullOrWhiteSpace(searchCriteria)) return hitFields;

			var searchTerms = searchCriteria.Trim().Split(new[] { ' ' });
			var properties = user.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (var searchTerm in searchTerms)
			{
				foreach (var propertyInfo in properties)
				{
					if (!(propertyInfo.PropertyType == typeof(string) || propertyInfo.PropertyType == typeof(DateTime)))
						continue;

					if (!columns.Contains(SearchUserColumns.All))
					{
						#region swicthforprofilecriteriacheck
						switch ((SearchUserProperties)Enum.Parse(typeof(SearchUserProperties), propertyInfo.Name))
						{
							case SearchUserProperties.FirstName:
								if (!columns.Contains(SearchUserColumns.FirstName)) continue;
								break;
							case SearchUserProperties.FullName:
								if (!columns.Contains(SearchUserColumns.FullName)) continue;
								break;
							case SearchUserProperties.LastName:
								if (!columns.Contains(SearchUserColumns.LastName)) continue;
								break;
							case SearchUserProperties.MiddleName:
								if (!columns.Contains(SearchUserColumns.MiddleName)) continue;
								break;
							case SearchUserProperties.NickName:
								if (!columns.Contains(SearchUserColumns.NickName)) continue;
								break;
							//case SearchUserProperties.DisplayName:
							//	if (!columns.Contains(SearchUserColumns.DisplayName)) continue;
							//	break;
							case SearchUserProperties.UserName:
								if (!columns.Contains(SearchUserColumns.UserName)) continue;
								break;
							case SearchUserProperties.EmailAddress:
								if (!columns.Contains(SearchUserColumns.EmailAddress)) continue;
								break;
							case SearchUserProperties.SecondaryEmailAddress:
								if (!columns.Contains(SearchUserColumns.EmailAddress)) continue;
								break;
							case SearchUserProperties.BirthDay:
								if (!columns.Contains(SearchUserColumns.BirthDay)) continue;
								break;
							case SearchUserProperties.BirthDate:
								continue;
							case SearchUserProperties.IsMarried:
								continue;
							case SearchUserProperties.Address:
								if (!columns.Contains(SearchUserColumns.Address)) continue;
								break;
							case SearchUserProperties.HomeTown:
								if (!columns.Contains(SearchUserColumns.HomeTown)) continue;
								break;
							case SearchUserProperties.FaxNumber:
								if (!columns.Contains(SearchUserColumns.FaxNumber)) continue;
								break;
							case SearchUserProperties.HomePhone:
								if (!columns.Contains(SearchUserColumns.HomePhone)) continue;
								break;
							case SearchUserProperties.CellPhone:
								if (!columns.Contains(SearchUserColumns.CellPhone)) continue;
								break;
							case SearchUserProperties.MobileDeviceType:
								if (!columns.Contains(SearchUserColumns.MobileDeviceType)) continue;
								break;
							case SearchUserProperties.UserId:
								if (!columns.Contains(SearchUserColumns.UserId)) continue;
								break;
							case SearchUserProperties.UserTypeId:
								continue;
							case SearchUserProperties.IsDeleted:
								continue;
							case SearchUserProperties.Location:
								if (!columns.Contains(SearchUserColumns.Location)) continue;
								break;
							case SearchUserProperties.BusinessUnit:
								if (!columns.Contains(SearchUserColumns.BusinessUnit)) continue;
								break;
							case SearchUserProperties.JobCode:
								if (!columns.Contains(SearchUserColumns.JobCode)) continue;
								break;
							case SearchUserProperties.Title:
								if (!columns.Contains(SearchUserColumns.Title)) continue;
								break;
							case SearchUserProperties.WorkPhone:
								if (!columns.Contains(SearchUserColumns.WorkPhone)) continue;
								break;
							case SearchUserProperties.Department:
								if (!columns.Contains(SearchUserColumns.Department)) continue;
								break;
							case SearchUserProperties.Project1:
							case SearchUserProperties.Project2:
							case SearchUserProperties.Project3:
								if (!columns.Contains(SearchUserColumns.Projects)) continue;
								break;
							case SearchUserProperties.WorkExpertise1:
							case SearchUserProperties.WorkExpertise2:
							case SearchUserProperties.WorkExpertise3:
								if (!columns.Contains(SearchUserColumns.WorkExpertise)) continue;
								break;
							case SearchUserProperties.UserPerspectives1:
							case SearchUserProperties.UserPerspectives2:
							case SearchUserProperties.UserPerspectives3:
								if (!columns.Contains(SearchUserColumns.UserPerspectives)) continue;
								break;
							case SearchUserProperties.AreasOfResponsibilities1:
							case SearchUserProperties.AreasOfResponsibilities2:
							case SearchUserProperties.AreasOfResponsibilities3:
								if (!columns.Contains(SearchUserColumns.AreasOfResponsibilities)) continue;
								break;
							case SearchUserProperties.Priorities1:
							case SearchUserProperties.Priorities2:
							case SearchUserProperties.Priorities3:
								if (!columns.Contains(SearchUserColumns.Priorities)) continue;
								break;
							case SearchUserProperties.AboutMe:
								if (!columns.Contains(SearchUserColumns.AboutMe)) continue;
								break;
							case SearchUserProperties.Activities:
								if (!columns.Contains(SearchUserColumns.Activities)) continue;
								break;
							case SearchUserProperties.Interests:
								if (!columns.Contains(SearchUserColumns.Interests)) continue;
								break;
							case SearchUserProperties.FavoriteBooks:
								if (!columns.Contains(SearchUserColumns.FavoriteBooks)) continue;
								break;
							case SearchUserProperties.FavoriteMovies:
								if (!columns.Contains(SearchUserColumns.FavoriteMovies)) continue;
								break;
							case SearchUserProperties.FavoriteMusic:
								if (!columns.Contains(SearchUserColumns.FavoriteMusic)) continue;
								break;
							case SearchUserProperties.FavoriteQuotations:
								if (!columns.Contains(SearchUserColumns.FavoriteQuotations)) continue;
								break;
							case SearchUserProperties.FavoriteTVShows:
								if (!columns.Contains(SearchUserColumns.FavoriteTVShows)) continue;
								break;
							case SearchUserProperties.SchoolId1:
							case SearchUserProperties.SchoolId2:
							case SearchUserProperties.SchoolId3:
								continue;
							case SearchUserProperties.SchoolName1:
							case SearchUserProperties.SchoolName2:
							case SearchUserProperties.SchoolName3:
								if (!columns.Contains(SearchUserColumns.SchoolName)) continue;
								break;
							case SearchUserProperties.ClassYearFrom1:
							case SearchUserProperties.ClassYearFrom2:
							case SearchUserProperties.ClassYearFrom3:
								if (!columns.Contains(SearchUserColumns.ClassYearFrom)) continue;
								break;
							case SearchUserProperties.ClassYearTo1:
							case SearchUserProperties.ClassYearTo2:
							case SearchUserProperties.ClassYearTo3:
								if (!columns.Contains(SearchUserColumns.ClassYearTo)) continue;
								break;
							case SearchUserProperties.Concentration1:
							case SearchUserProperties.Concentration2:
							case SearchUserProperties.Concentration3:
								if (!columns.Contains(SearchUserColumns.Concentration)) continue;
								break;
							case SearchUserProperties.Degree1:
							case SearchUserProperties.Degree2:
							case SearchUserProperties.Degree3:
								if (!columns.Contains(SearchUserColumns.Degree)) continue;
								break;
							case SearchUserProperties.EmployerId1:
							case SearchUserProperties.EmployerId2:
							case SearchUserProperties.EmployerId3:
								continue;
							case SearchUserProperties.EmployerName1:
							case SearchUserProperties.EmployerName2:
							case SearchUserProperties.EmployerName3:
								if (!columns.Contains(SearchUserColumns.EmployerName)) continue;
								break;
							case SearchUserProperties.Position1:
							case SearchUserProperties.Position2:
							case SearchUserProperties.Position3:
								if (!columns.Contains(SearchUserColumns.Position)) continue;
								break;
							case SearchUserProperties.City1:
							case SearchUserProperties.City2:
							case SearchUserProperties.City3:
								if (!columns.Contains(SearchUserColumns.City)) continue;
								break;
							case SearchUserProperties.YearFrom1:
							case SearchUserProperties.YearFrom2:
							case SearchUserProperties.YearFrom3:
								if (!columns.Contains(SearchUserColumns.YearFrom)) continue;
								break;
							case SearchUserProperties.YearTo1:
							case SearchUserProperties.YearTo2:
							case SearchUserProperties.YearTo3:
								if (!columns.Contains(SearchUserColumns.YearTo)) continue;
								break;
							case SearchUserProperties.Responsibilities1:
							case SearchUserProperties.Responsibilities2:
							case SearchUserProperties.Responsibilities3:
								if (!columns.Contains(SearchUserColumns.Responsibilities)) continue;
								break;
						}
					}
					#endregion

					if (advancedSearch)
					{
						if (!(propertyInfo.Name == "Location" || propertyInfo.Name == "BusinessUnit" || propertyInfo.Name == "Department"))
							continue;
					}
					var value = propertyInfo.GetValue(user, null);
					if (value != null && value.ToString().ToLowerInvariant().Contains(searchTerm.ToLowerInvariant()))
					{
						if (!hitFields.Keys.Contains(propertyInfo.Name))
							hitFields.Add(propertyInfo.Name, propertyInfo.GetValue(user, null).ToString());
					}
				}
			}

			if (hitFields.Keys.Contains("FirstName"))
			{
				hitFields.Add("First Name", hitFields["FirstName"]);
				hitFields.Remove("FirstName");
			}

			if (hitFields.Keys.Contains("LastName"))
			{
				hitFields.Add("Last Name", hitFields["LastName"]);
				hitFields.Remove("LastName");
			}

			if (hitFields.Keys.Contains("UserName"))
			{
				hitFields.Add("User Name", hitFields["UserName"]);
				hitFields.Remove("UserName");
			}

			if (hitFields.Keys.Contains("FullName"))
			{
				hitFields.Add("Full Name", hitFields["FullName"]);
				hitFields.Remove("FullName");
			}

			//if (hitFields.Keys.Contains("DisplayName"))
			//{
			//	hitFields.Add("Display Name", hitFields["DisplayName"]);
			//	hitFields.Remove("DisplayName");
			//}

			if (hitFields.Keys.Contains("EmailAddress"))
			{
				hitFields.Add("Email Address", hitFields["EmailAddress"]);
				hitFields.Remove("EmailAddress");
			}

			if (hitFields.Keys.Contains("Project1") || hitFields.Keys.Contains("Project2") || hitFields.Keys.Contains("Project3"))
			{
				hitFields.Add("Projects", string.Join(", ", new List<string> {hitFields.Keys.Contains("Project1") ? hitFields["Project1"] : string.Empty,
																				hitFields.Keys.Contains("Project2") ? hitFields["Project2"] : string.Empty,
																				hitFields.Keys.Contains("Project3") ? hitFields["Project3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("Project1");
				hitFields.Remove("Project2");
				hitFields.Remove("Project3");
			}

			if (hitFields.Keys.Contains("WorkExpertise1") || hitFields.Keys.Contains("WorkExpertise2") || hitFields.Keys.Contains("WorkExpertise3"))
			{
				hitFields.Add("WorkExpertise", string.Join(", ", new List<string> {hitFields.Keys.Contains("WorkExpertise1") ? hitFields["WorkExpertise1"] : string.Empty,
																				hitFields.Keys.Contains("WorkExpertise2") ? hitFields["WorkExpertise2"] : string.Empty,
																				hitFields.Keys.Contains("WorkExpertise3") ? hitFields["WorkExpertise3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("WorkExpertise1");
				hitFields.Remove("WorkExpertise2");
				hitFields.Remove("WorkExpertise3");
			}

			if (hitFields.Keys.Contains("UserPerspectives1") || hitFields.Keys.Contains("UserPerspectives2") || hitFields.Keys.Contains("UserPerspectives3"))
			{
				hitFields.Add("UserPerspectives", string.Join(", ", new List<string> {hitFields.Keys.Contains("UserPerspectives1") ? hitFields["UserPerspectives1"] : string.Empty,
																				hitFields.Keys.Contains("UserPerspectives2") ? hitFields["UserPerspectives2"] : string.Empty,
																				hitFields.Keys.Contains("UserPerspectives3") ? hitFields["UserPerspectives3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("UserPerspectives1");
				hitFields.Remove("UserPerspectives2");
				hitFields.Remove("UserPerspectives3");
			}

			if (hitFields.Keys.Contains("AreasOfResponsibilities1") || hitFields.Keys.Contains("AreasOfResponsibilities2") || hitFields.Keys.Contains("AreasOfResponsibilities3"))
			{
				hitFields.Add("AreasOfResponsibilities", string.Join(", ", new List<string> {hitFields.Keys.Contains("AreasOfResponsibilities1") ? hitFields["AreasOfResponsibilities1"] : string.Empty,
																				hitFields.Keys.Contains("AreasOfResponsibilities2") ? hitFields["AreasOfResponsibilities2"] : string.Empty,
																				hitFields.Keys.Contains("AreasOfResponsibilities3") ? hitFields["AreasOfResponsibilities3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("AreasOfResponsibilities1");
				hitFields.Remove("AreasOfResponsibilities2");
				hitFields.Remove("AreasOfResponsibilities3");
			}

			if (hitFields.Keys.Contains("Priorities1") || hitFields.Keys.Contains("Priorities2") || hitFields.Keys.Contains("Priorities3"))
			{
				hitFields.Add("Priorities", string.Join(", ", new List<string> {hitFields.Keys.Contains("Priorities1") ? hitFields["Priorities1"] : string.Empty,
																				hitFields.Keys.Contains("Priorities2") ? hitFields["Priorities2"] : string.Empty,
																				hitFields.Keys.Contains("Priorities3") ? hitFields["Priorities3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("Priorities1");
				hitFields.Remove("Priorities2");
				hitFields.Remove("Priorities3");
			}

			if (hitFields.Keys.Contains("SchoolName1") || hitFields.Keys.Contains("SchoolName2") || hitFields.Keys.Contains("SchoolName3"))
			{
				hitFields.Add("SchoolName", string.Join(", ", new List<string> {hitFields.Keys.Contains("SchoolName1") ? hitFields["SchoolName1"] : string.Empty,
																				hitFields.Keys.Contains("SchoolName2") ? hitFields["SchoolName2"] : string.Empty,
																				hitFields.Keys.Contains("SchoolName3") ? hitFields["SchoolName3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("SchoolName1");
				hitFields.Remove("SchoolName2");
				hitFields.Remove("SchoolName3");
			}


			if (hitFields.Keys.Contains("ClassYearFrom1") || hitFields.Keys.Contains("ClassYearFrom2") || hitFields.Keys.Contains("ClassYearFrom3"))
			{
				hitFields.Add("ClassYearFrom", string.Join(", ", new List<string> {hitFields.Keys.Contains("ClassYearFrom1") ? hitFields["ClassYearFrom1"] : string.Empty,
																				hitFields.Keys.Contains("ClassYearFrom2") ? hitFields["ClassYearFrom2"] : string.Empty,
																				hitFields.Keys.Contains("ClassYearFrom3") ? hitFields["ClassYearFrom3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("ClassYearFrom1");
				hitFields.Remove("ClassYearFrom2");
				hitFields.Remove("ClassYearFrom3");
			}

			if (hitFields.Keys.Contains("ClassYearTo1") || hitFields.Keys.Contains("ClassYearTo2") || hitFields.Keys.Contains("ClassYearTo3"))
			{
				hitFields.Add("ClassYearTo", string.Join(", ", new List<string> {hitFields.Keys.Contains("ClassYearTo1") ? hitFields["ClassYearTo1"] : string.Empty,
																				hitFields.Keys.Contains("ClassYearTo2") ? hitFields["ClassYearTo2"] : string.Empty,
																				hitFields.Keys.Contains("ClassYearTo3") ? hitFields["ClassYearTo3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("ClassYearTo1");
				hitFields.Remove("ClassYearTo2");
				hitFields.Remove("ClassYearTo3");
			}

			if (hitFields.Keys.Contains("Concentration1") || hitFields.Keys.Contains("Concentration2") || hitFields.Keys.Contains("Concentration3"))
			{
				hitFields.Add("Concentration", string.Join(", ", new List<string> {hitFields.Keys.Contains("Concentration1") ? hitFields["Concentration1"] : string.Empty,
																				hitFields.Keys.Contains("Concentration2") ? hitFields["Concentration2"] : string.Empty,
																				hitFields.Keys.Contains("Concentration3") ? hitFields["Concentration3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("Concentration1");
				hitFields.Remove("Concentration2");
				hitFields.Remove("Concentration3");
			}

			if (hitFields.Keys.Contains("Degree1") || hitFields.Keys.Contains("Degree2") || hitFields.Keys.Contains("Degree3"))
			{
				hitFields.Add("Degree", string.Join(", ", new List<string> {hitFields.Keys.Contains("Degree1") ? hitFields["Degree1"] : string.Empty,
																				hitFields.Keys.Contains("Degree2") ? hitFields["Degree2"] : string.Empty,
																				hitFields.Keys.Contains("Degree3") ? hitFields["Degree3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("Degree1");
				hitFields.Remove("Degree2");
				hitFields.Remove("Degree3");
			}

			if (hitFields.Keys.Contains("EmployerName1") || hitFields.Keys.Contains("EmployerName2") || hitFields.Keys.Contains("EmployerName3"))
			{
				hitFields.Add("EmployerName", string.Join(", ", new List<string> {hitFields.Keys.Contains("EmployerName1") ? hitFields["EmployerName1"] : string.Empty,
																				hitFields.Keys.Contains("EmployerName2") ? hitFields["EmployerName2"] : string.Empty,
																				hitFields.Keys.Contains("EmployerName3") ? hitFields["EmployerName3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("EmployerName1");
				hitFields.Remove("EmployerName2");
				hitFields.Remove("EmployerName3");
			}

			if (hitFields.Keys.Contains("Position1") || hitFields.Keys.Contains("Position2") || hitFields.Keys.Contains("Position3"))
			{
				hitFields.Add("Position", string.Join(", ", new List<string> {hitFields.Keys.Contains("Position1") ? hitFields["Position1"] : string.Empty,
																				hitFields.Keys.Contains("Position2") ? hitFields["Position2"] : string.Empty,
																				hitFields.Keys.Contains("Position3") ? hitFields["Position3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("Position1");
				hitFields.Remove("Position2");
				hitFields.Remove("Position3");
			}

			if (hitFields.Keys.Contains("City1") || hitFields.Keys.Contains("City2") || hitFields.Keys.Contains("City3"))
			{
				hitFields.Add("Job City", string.Join(", ", new List<string> {hitFields.Keys.Contains("City1") ? hitFields["City1"] : string.Empty,
																				hitFields.Keys.Contains("City2") ? hitFields["City2"] : string.Empty,
																				hitFields.Keys.Contains("City3") ? hitFields["City3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("City1");
				hitFields.Remove("City2");
				hitFields.Remove("City3");
			}

			if (hitFields.Keys.Contains("YearFrom1") || hitFields.Keys.Contains("YearFrom2") || hitFields.Keys.Contains("YearFrom3"))
			{
				hitFields.Add("Job YearFrom", string.Join(", ", new List<string> {hitFields.Keys.Contains("YearFrom1") ? hitFields["YearFrom1"] : string.Empty,
																				hitFields.Keys.Contains("YearFrom2") ? hitFields["YearFrom2"] : string.Empty,
																				hitFields.Keys.Contains("YearFrom3") ? hitFields["YearFrom3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("YearFrom1");
				hitFields.Remove("YearFrom2");
				hitFields.Remove("YearFrom3");
			}

			if (hitFields.Keys.Contains("YearTo1") || hitFields.Keys.Contains("YearTo2") || hitFields.Keys.Contains("YearTo3"))
			{
				hitFields.Add("Job YearTo", string.Join(", ", new List<string> {hitFields.Keys.Contains("YearTo1") ? hitFields["YearTo1"] : string.Empty,
																				hitFields.Keys.Contains("YearTo2") ? hitFields["YearTo2"] : string.Empty,
																				hitFields.Keys.Contains("YearTo3") ? hitFields["YearTo3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("YearTo1");
				hitFields.Remove("YearTo2");
				hitFields.Remove("YearTo3");
			}

			if (hitFields.Keys.Contains("Responsibilities1") || hitFields.Keys.Contains("Responsibilities2") || hitFields.Keys.Contains("Responsibilities3"))
			{
				hitFields.Add("Job Responsibilities", string.Join(", ", new List<string> {hitFields.Keys.Contains("Responsibilities1") ? hitFields["Responsibilities1"] : string.Empty,
																				hitFields.Keys.Contains("Responsibilities2") ? hitFields["Responsibilities2"] : string.Empty,
																				hitFields.Keys.Contains("Responsibilities3") ? hitFields["Responsibilities3"] : string.Empty}.Where(s => !string.IsNullOrWhiteSpace(s))));
				hitFields.Remove("Responsibilities1");
				hitFields.Remove("Responsibilities2");
				hitFields.Remove("Responsibilities3");
			}

			if (hitFields.Keys.Contains("SchoolId1")) hitFields.Remove("SchoolId1");
			if (hitFields.Keys.Contains("SchoolId2")) hitFields.Remove("SchoolId2");
			if (hitFields.Keys.Contains("SchoolId3")) hitFields.Remove("SchoolId3");
			if (hitFields.Keys.Contains("EmployerId1")) hitFields.Remove("EmployerId1");
			if (hitFields.Keys.Contains("EmployerId2")) hitFields.Remove("EmployerId2");
			if (hitFields.Keys.Contains("EmployerId3")) hitFields.Remove("EmployerId3");
		 
			return hitFields;
		}

		private string GetPrivateGroups()
		{
			var privateGroups = Flow.Group.MyPrivateGroupList(ApplicationId, UserId);
			return privateGroups.Any() ? string.Join(",", privateGroups.Select(group => @group.GroupId).ToArray()) : string.Empty;
		}

		private SearchModel SetAllSearchProfileColumns(SearchModel model)
		{
			var properties = model.AdvancedSearch.ProfileSearch.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (var propertyInfo in properties)
			{
				if (!(propertyInfo.PropertyType == typeof(bool)))
					continue;

				if (propertyInfo.Name == "ProfileSearchText")
					continue;

				propertyInfo.SetValue(model.AdvancedSearch.ProfileSearch, true);
			}

			return model;
		}

		private List<SearchUserColumns> GetProfileSearchColumns(SearchModel model)
		{
			var searchColumns = new List<SearchUserColumns>();
			model.ProfileCriteria = new List<string>();
			if (model.AdvancedSearch == null)
			{
				searchColumns.Add(SearchUserColumns.All);
				return searchColumns;
			}
			//Profile Information
			if (model.AdvancedSearch.ProfileSearch.SearchJobTitle) { searchColumns.Add(SearchUserColumns.Title); model.ProfileCriteria.Add(SearchUserColumns.Title.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchLocation) { searchColumns.Add(SearchUserColumns.Location); model.ProfileCriteria.Add(SearchUserColumns.Location.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchProjects) { searchColumns.Add(SearchUserColumns.Projects); model.ProfileCriteria.Add(SearchUserColumns.Projects.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchResp) { searchColumns.Add(SearchUserColumns.AreasOfResponsibilities); model.ProfileCriteria.Add(SearchUserColumns.AreasOfResponsibilities.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchExpertise) { searchColumns.Add(SearchUserColumns.WorkExpertise); model.ProfileCriteria.Add(SearchUserColumns.WorkExpertise.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchWorkPhone) { searchColumns.Add(SearchUserColumns.WorkPhone); model.ProfileCriteria.Add(SearchUserColumns.WorkPhone.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchCellPhone) { searchColumns.Add(SearchUserColumns.CellPhone); model.ProfileCriteria.Add(SearchUserColumns.CellPhone.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchFax) { searchColumns.Add(SearchUserColumns.FaxNumber); model.ProfileCriteria.Add(SearchUserColumns.FaxNumber.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchMobileDeviceType) { searchColumns.Add(SearchUserColumns.MobileDeviceType); model.ProfileCriteria.Add(SearchUserColumns.MobileDeviceType.ToString()); }

			//Personal Information
			if (model.AdvancedSearch.ProfileSearch.SearchActivities) { searchColumns.Add(SearchUserColumns.Activities); model.ProfileCriteria.Add(SearchUserColumns.Activities.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchInterests) { searchColumns.Add(SearchUserColumns.Interests); model.ProfileCriteria.Add(SearchUserColumns.Interests.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchAboutMe) { searchColumns.Add(SearchUserColumns.AboutMe); model.ProfileCriteria.Add(SearchUserColumns.AboutMe.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchBirthday) { searchColumns.Add(SearchUserColumns.BirthDay); model.ProfileCriteria.Add(SearchUserColumns.BirthDay.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchHometown) { searchColumns.Add(SearchUserColumns.HomeTown); model.ProfileCriteria.Add(SearchUserColumns.HomeTown.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchTvShows) { searchColumns.Add(SearchUserColumns.FavoriteTVShows); model.ProfileCriteria.Add(SearchUserColumns.FavoriteTVShows.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchMusic) { searchColumns.Add(SearchUserColumns.FavoriteMusic); model.ProfileCriteria.Add(SearchUserColumns.FavoriteMusic.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchMovies) { searchColumns.Add(SearchUserColumns.FavoriteMovies); model.ProfileCriteria.Add(SearchUserColumns.FavoriteMovies.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchBooks) { searchColumns.Add(SearchUserColumns.FavoriteBooks); model.ProfileCriteria.Add(SearchUserColumns.FavoriteBooks.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchQuotes) { searchColumns.Add(SearchUserColumns.FavoriteQuotations); model.ProfileCriteria.Add(SearchUserColumns.FavoriteQuotations.ToString()); }

			//Work History
			if (model.AdvancedSearch.ProfileSearch.SearchEmpName) { searchColumns.Add(SearchUserColumns.EmployerName); model.ProfileCriteria.Add(SearchUserColumns.EmployerName.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchEmpCity) { searchColumns.Add(SearchUserColumns.City); model.ProfileCriteria.Add(SearchUserColumns.City.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchWHPosition) { searchColumns.Add(SearchUserColumns.Position); model.ProfileCriteria.Add(SearchUserColumns.Position.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchWHTimePeriod) { searchColumns.Add(SearchUserColumns.YearFrom); searchColumns.Add(SearchUserColumns.YearTo); model.ProfileCriteria.Add(Resources.Search.EmployementTime); }
			if (model.AdvancedSearch.ProfileSearch.SearchWHResp) { searchColumns.Add(SearchUserColumns.Responsibilities); model.ProfileCriteria.Add(SearchUserColumns.Responsibilities.ToString()); }

			//School History
			if (model.AdvancedSearch.ProfileSearch.SearchSchoolName) { searchColumns.Add(SearchUserColumns.SchoolName); model.ProfileCriteria.Add(SearchUserColumns.SchoolName.ToString()); }
			if (model.AdvancedSearch.ProfileSearch.SearchClassYear) { searchColumns.Add(SearchUserColumns.ClassYearFrom); searchColumns.Add(SearchUserColumns.ClassYearTo); model.ProfileCriteria.Add(Resources.Search.ClassYear); }
			if (model.AdvancedSearch.ProfileSearch.SearchConcentration) { searchColumns.Add(SearchUserColumns.Concentration); model.ProfileCriteria.Add(SearchUserColumns.Concentration.ToString()); }

			return searchColumns;
		}
		#endregion
	}
}